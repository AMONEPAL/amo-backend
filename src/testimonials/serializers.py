from rest_framework import serializers
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from .models import Testimonial

class TestimonialSerializer(serializers.ModelSerializer):
	class Meta:
		model = Testimonial
		fields= '__all__'




