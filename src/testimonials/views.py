from rest_framework import viewsets
from . import models
from .serializers import TestimonialSerializer

class TestimonialViewset(viewsets.ModelViewSet):
    queryset = models.Testimonial.objects.all()
    serializer_class = TestimonialSerializer