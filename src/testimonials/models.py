from django.db import models

class Testimonial(models.Model):
	name = models.CharField(max_length=500, null=True, blank=True)
	description = models.CharField(max_length=500, null=True, blank=True)
	company = models.CharField(max_length=500, null=True, blank=True)
	location = models.CharField(max_length=500, null=True, blank=True)
	photo = models.ImageField(upload_to='testimonial_photos', null=True, blank = True)
	created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

	def __str__(self):
		return self.name

# Create your models here.