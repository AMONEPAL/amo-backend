# from django.urls import path
# from django.conf.urls import url
# from .views import *
# from toofanapp import views

# app_name = 'toofanapp'
# urlpatterns = [
#     path('', IndexView.as_view(), name='index'),
# ]

from django.urls import path, re_path
from .views import *

urlpatterns = [
    path('', IndexView, name='index'),
    path('product/', ProductView.as_view(), name='product'),
    # path('product-detail/', ProductDetailView.as_view(), name='product-detail'),
    # path(r'product/(?P<slug><pk>\w+)/$',
    #      ProductDetailView.as_view(), name='product-detail'),
    path('product/<int:product_id>/<str:title>', ProductDetailView, name='product-detail'),
    path('signup/', SignupView.as_view(), name='sign-up'),
    path('signin/', SigninView.as_view(), name='sign-in'),
    path('resetpassword/', ForgetPasswordView.as_view(), name='forget-password'),

]