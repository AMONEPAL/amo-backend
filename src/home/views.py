from django.shortcuts import render
from .models import *
from django.views.generic import TemplateView, View, FormView, ListView, DetailView
from products.models import Product, Category, Brand
from products.filters import ProductFilter
from slider.models import Slider
from django.shortcuts import render, get_object_or_404
from random import choice 
from testimonials.models import Testimonial

# Create your views here.


def IndexView(request):
    template = 'home/frontend/home.html'
    Sliders = Slider.objects.all()
    esn_products_asc = Product.objects.order_by('-id')[:10] #explore_something_new_products
    # esn_products_asc = reversed(esn_products) # assending order
    trending_products =  Product.objects.order_by('id')[:10] #choice(Product.objects.all()[:10])
    # print(esn_products_asc)
    popular_categories = Category.objects.order_by('id')[:6] 

    testomonials = Testimonial.objects.all()
    brands = Brand.objects.all()
    ctx = {
        'sliders': Sliders,
        'esn_products_asc': esn_products_asc,
        'trending_products': trending_products,
        'popular_categories': popular_categories,
        'testomonials' : testomonials,
        'brands' : brands
        }
    return render(request, template, ctx)

# class IndexView(ListView):
#     template_name = 'home/frontend/home.html'
#     model = Product

#     def get_context_data(self, **kwargs):
#         # cat = Category.objects.all()
#         # cat_ar = []
#         # for a in cat:
#         #     for b in cat:
#         #         if b.fk_category_id == a.id:
#         #             cat_ar = cat_ar.append(b, a)
#         menu = dict()
#         p_cats = Category.objects.filter(fk_category_id=None).all()
#         for p_cat in p_cats:
#             print(p_cat.id)
#             menu[p_cat.id] =dict()
#             menu[p_cat.id]['parent']=p_cat
#             menu[p_cat.id]['child']=[]
#             children = Category.objects.filter(fk_category_id=p_cat.id).all()
#             for child in children:
#                 print([p_cat.id, child.id])
#                 menu[p_cat.id]['child'].append(child) 

#         print(menu)



#         context = super(IndexView, self).get_context_data(**kwargs)
#         context['products'] = Product.objects.all()
#         context['top_selling_product'] = Product.objects.all()[0:5]
#         context['featured_collection_products'] = Product.objects.all()[0:4]
#         context['new_arrival_products'] =Product.objects.order_by('-id')[0:4][::-1]
#         context['shop_below_999'] = Product.objects.filter(price__lt=999)[0:4]
#         context['categories'] = Category.objects.all()[0:4]
#         context['productImage'] = []
#         context['sliders'] = Slider.objects.all()
#         # daily_product_id = []
#         # for d in daily_products:
#         #     product_id = d.product.id
#         #     if product_id not in daily_product_id:
#         #         context['productImage'].append(d)
#         #         daily_product_id.append(product_id)
#         return context


# class ProductDetailView(ListView):
#     def get(self,request):
#         product_id = self.request.GET.get('pk')
#         print(product_id)


#     template_name = 'home/frontend/ProductDetailView.html'

class ProductView(TemplateView):
    template_name = 'home/product/product.html'


def ProductDetailView(request, product_id, title):

    #product_instance = Product.objects.get(id=id)
    product_instance = get_object_or_404(Product, pk=product_id)
    try:
        product_instance = Product.objects.get(pk=product_id)
    except Product.DoesNotExist:
        raise Http404
    except:
        raise Http404
    # product_ids = []
    # similar_product_category  = product_instance.categories.all().first().id
    # product_ids_in_category = Category.objects.filter(category_id=similar_product_category)
    # for obj in product_ids_in_category:
    #     product_ids = product_ids.append(obj.product_id)
    # similar_products = Product.objects.filter(pk__in=product_ids)
    # print(similar_products)
    
    template = 'home/product/product-detail.html'
    context = { 
        "object": product_instance
    }
    return render(request, template, context)


    # model = Product

    # def get_context_data(self, **kwargs):
    #     context = super(ProductDetailView, self).get_context_data(**kwargs)
    #     context['product_detail'] = Product.objects.get(
    #         id=str(kwargs.get('pk')))
    #     context['product_images'] = context['product_detail'].productimage_set.all()
    #     return context

class SignupView(TemplateView):
    template_name = 'home/auth/signup.html'

class SigninView(TemplateView):
    template_name = 'home/auth/signin.html'

class ForgetPasswordView(TemplateView):
    template_name = 'home/auth/forgetpassword.html'
