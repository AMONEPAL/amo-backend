from .models import GuestCart

from rest_framework import serializers
from products.serializers import VariationSerializer, ProductSerializer
from .services import GetGuestCartSessionId
class GuestCartListSerializer(serializers.ModelSerializer):
	fk_variation = VariationSerializer()
	class Meta:
		model = GuestCart
		fields = '__all__'


class GuestCartSerializer(serializers.ModelSerializer):
	class Meta:
		model = GuestCart
		fields = '__all__'
	
	def create(self, validated_data):
		instance = self.Meta.model(**validated_data)
		# instance.session_id = self.request.COOKIES.get('session_id')
		instance.session_id = GetGuestCartSessionId(self.context['request'])
		instance.save()
		return instance

	# def update(self, instance, validated_data):
	# 	for attr, value in validated_data.items():
	# 		if attr == 'password':
	# 			instance.set_password(value)
	# 		else:
	# 			setattr(instance, attr, value)
	# 	instance.save()
	# 	return instance
		# def create(self, validated_data):