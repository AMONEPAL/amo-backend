from django.apps import AppConfig


class GuestcartsConfig(AppConfig):
    name = 'guestcarts'
