from django.db import models
from products.models import *

# Create your models here.
class GuestCart(models.Model):
	fk_variation = models.ForeignKey(Variation, on_delete=models.CASCADE,)
	quantity = models.PositiveIntegerField(default=1)
	price = models.PositiveIntegerField(default=0.0)
	session_id = models.CharField(max_length=100, blank=True)
	phone_number = models.CharField(max_length=100, blank=True)
	email = models.EmailField()
	ordered_date = models.DateTimeField(auto_now_add=True, auto_now=False)
	timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)

	def __str__(self):
		return self.session_id