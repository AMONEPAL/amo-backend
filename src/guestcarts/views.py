from django.shortcuts import render

# Create your views here.
from .models import GuestCart

from rest_framework.authentication import SessionAuthentication
from rest_framework.generics import CreateAPIView, ListAPIView,ListCreateAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
# from .serializer import CreateUserSerializer
from django.http import HttpResponse
from django.views.generic import TemplateView, View, FormView, ListView, DetailView

from .serializers import GuestCartSerializer, GuestCartListSerializer
from django.contrib.auth import get_user_model
import uuid
from django.shortcuts import get_object_or_404 
import random
import requests
from django.core.mail import send_mail
from .services import GetGuestCartSessionId


class GuestCartListApiView(ListAPIView):
    serializer_class = GuestCartListSerializer

    def get_queryset(self, *args, **kwargs):
        #return Order.objects.filter(user__user=self.request.user)
        session_id = GetGuestCartSessionId(self.request)
        return GuestCart.objects.filter(session_id=session_id)

class GuestCartCreateApiView(CreateAPIView):
    serializer_class = GuestCartSerializer

    def get_queryset(self, *args, **kwargs):
        #return Order.objects.filter(user__user=self.request.user)
        return GuestCart.objects.all()

#http://localhost:8000/api/membership-type/<pk>/
#
class GuestCartRetrieveUpdateDestroyApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = GuestCartSerializer
    def get_queryset(self, *args, **kwargs):
        return GuestCart.objects.all()



class GuestCartView(TemplateView):
    template_name = 'guestcarts/guestcart.html'


class GuestCartCheckoutAPIView(APIView):
	def post(self, request):
		# guestcart = GuestCart()
		phone_number = request.data.get('phone_number')
		email = request.data.get('email')
		session_id = GetGuestCartSessionId(request)
		guestcarts = GuestCart.objects.filter(session_id=session_id)
		for guestcart in guestcarts:
			guestcart.email = email
			guestcart.phone_number = phone_number
			guestcart.save()
		return Response({
			'status': True,
			'detail': 'eat arybedic ghas'
			})




