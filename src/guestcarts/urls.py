from django.urls import path
from .views import GuestCartCreateApiView, GuestCartListApiView, GuestCartView, GuestCartRetrieveUpdateDestroyApiView, GuestCartCheckoutAPIView

app_name = 'guestcarts'

urlpatterns = [
    path('api/guestcart/', GuestCartCreateApiView.as_view(), name="ap-guestcart"),
    path('api/guestcart/<int:pk>', GuestCartRetrieveUpdateDestroyApiView.as_view(), name="ap-guestcart-update"),
    path('api/guestcartlist/', GuestCartListApiView.as_view(), name="ap-guestcart-list"),
    path('guest_cart_view/', GuestCartView.as_view(), name="guest-guestcart"),
    path('api/guest_cart_checkout/', GuestCartCheckoutAPIView.as_view(), name="guestcart-checkout"),

 ]
