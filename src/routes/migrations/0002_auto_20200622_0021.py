# Generated by Django 2.2.2 on 2020-06-21 18:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0002_auto_20200613_1735'),
        ('routes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=500, null=True)),
                ('code', models.CharField(blank=True, max_length=500, null=True)),
                ('fk_store', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='fk_store_route', to='store.Store')),
            ],
        ),
        migrations.CreateModel(
            name='RouteDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_latitude', models.CharField(blank=True, max_length=200, null=True)),
                ('order_longitude', models.CharField(blank=True, max_length=200, null=True)),
                ('fk_route', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fk_route_route_detail', to='routes.Route')),
            ],
        ),
        migrations.DeleteModel(
            name='PaymentMethod',
        ),
    ]
