# Generated by Django 2.2.2 on 2020-06-18 16:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0009_storewiseorder'),
        ('carts', '0006_auto_20200602_2231'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartitem',
            name='fk_storewise_order',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='orders.StoreWiseOrder'),
        ),
    ]
