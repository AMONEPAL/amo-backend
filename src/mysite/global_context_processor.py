from testimonials.models import Testimonial
from products.models import Category

def testimonials(request):
	return {
		'gl_testimonials' : Testimonial.objects.all(),
		'categories': Category.objects.all() 
	}