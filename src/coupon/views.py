from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.exceptions import NotAcceptable, NotFound
from rest_framework import viewsets, permissions, generics, mixins, status

from .permission import IsSuperUserAndAuth
from .models import Coupon
from .serializers import CouponSerializer


class CouponViewSet(viewsets.ModelViewSet):
    serializer_class = CouponSerializer
    permission_classes = [IsSuperUserAndAuth]

    def get_queryset(self):
        return Coupon.objects.filter(is_active=True)
