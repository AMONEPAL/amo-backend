# Generated by Django 2.2.2 on 2020-10-02 13:33

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('coupon', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='coupon',
            name='created_at',
            field=models.DateField(auto_now_add=True, default=datetime.datetime(2020, 10, 2, 13, 32, 55, 490566, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='coupon',
            name='expiry_date',
            field=models.DateField(default='2020-09-09'),
            preserve_default=False,
        ),
    ]
