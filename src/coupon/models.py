from django.db import models

# Create your models here.

COUPON_TYPES = (
    ('flat', 'Flat'),
    ('percentage', 'Percentage')

)


class Coupon(models.Model):
    title = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    coupon_type = models.CharField(
        choices=COUPON_TYPES, default=1, max_length=100)
    amount = models.DecimalField(decimal_places=2, max_digits=5)
    created_at = models.DateField(auto_now_add=True)
    expiry_date = models.DateField(auto_now_add=False)
    is_active = models.BooleanField(default=True)
