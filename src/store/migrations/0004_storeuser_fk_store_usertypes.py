# Generated by Django 2.2.2 on 2020-06-23 17:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_auto_20200603_1528'),
        ('store', '0003_storeuser'),
    ]

    operations = [
        migrations.AddField(
            model_name='storeuser',
            name='fk_store_usertypes',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='users.UserTypes'),
        ),
    ]
