import React, { Fragment, useEffect, useState } from "react";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  Button,
  Space,
  PageHeader,
  Dropdown,
  Menu,
  Spin,
} from "antd";
import { Redirect } from "react-router-dom";
import SideNav from "../dashboard/SideNav";
import TopHeader from "../dashboard/TopHeader";
import { Link } from "react-router-dom";

const { Content } = Layout;

const ManageOrder = (props) => {
  const [orderID, setOrderID] = useState(null);
  const [orderDetailClicked, setOrderDetailClicked] = useState(false);

  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const columns = [
    {
      title: "Order Id",
      dataIndex: "orderid",
      key: "orderid",
      width: 100,
    },
    {
      title: "Created",
      dataIndex: "created",
      key: "created",
    },
    {
      title: "User",
      dataIndex: "user",
      key: "user",
    },
    {
      title: "Items",
      dataIndex: "items",
      key: "items",
    },
    {
      title: "Total Amount",
      dataIndex: "total_amount",
      key: "total_amount",
    },
    {
      title: "Shipping Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (tags) => (
        <>
          {tags.map((tag) => {
            let color;
            if (tag === "pending") {
              color = "red";
            }
            if (tag === "delivered") {
              color = "green";
            }
            if (tag === "paid") {
              color = "blue";
            }
            if (tag === "refunded") {
              color = "orange";
            }
            if (tag === "canceled") {
              color = "pink";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      width: 100,
      render: (text, record) => (
        <>
          <a onClick={() => handleOrderDetail()}>
            <i className="fa fa-eye"></i>
          </a>
        </>
      ),
    },
  ];

  const data = [
    {
      key: "1",
      orderid: 1000001,
      created: "2020-04-04",
      user: "Jackie Chain",
      items: 5,
      total_amount: 677,
      address: "Kathmandu, Nepal",
      status: ["pending"],
    },
    {
      key: "2",
      orderid: 1000001,
      created: "2020-04-04",
      user: "Jackie Chain",
      items: 5,
      total_amount: 677,
      address: "Kathmandu, Nepal",
      status: ["delivered"],
    },
    {
      key: "3",
      orderid: 1000001,
      created: "2020-04-04",
      user: "Jackie Chain",
      items: 5,
      total_amount: 677,
      address: "Kathmandu, Nepal",
      status: ["refunded"],
    },
    {
      key: "4",
      orderid: 1000001,
      created: "2020-04-04",
      user: "Jackie Chain",
      items: 5,
      total_amount: 677,
      address: "Kathmandu, Nepal",
      status: ["paid"],
    },
    {
      key: "4",
      orderid: 1000001,
      created: "2020-04-04",
      user: "Jackie Chain",
      items: 5,
      total_amount: 677,
      address: "Kathmandu, Nepal",
      status: ["canceled"],
    },
  ];

  const handleOrderDetail = () => {
    setOrderID(1);
    setOrderDetailClicked(true);
  };

  if (orderDetailClicked) {
    return <Redirect to={`/manage-orders/order/${orderID}`} />;
  }

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Orders</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-orders">Manage Orders</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Orders"
              className="site-page-header"
              extra={[
                <Button key="2" className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="table-section spacing">
              <div className="table-responsive">
                <Table bordered={true} columns={columns} dataSource={data} />
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default ManageOrder;
