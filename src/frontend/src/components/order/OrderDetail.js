import React, { useState } from "react";

import {
  Layout,
  Breadcrumb,
  Button,
  Select,
  PageHeader,
  Tabs,
  Dropdown,
  Menu,
  Modal,
  Form,
} from "antd";
const { Option } = Select;
import SideNav from "../dashboard/SideNav";
import TopHeader from "../dashboard/TopHeader";
import { Link } from "react-router-dom";

const { Content } = Layout;
const { TabPane } = Tabs;

const OrderDetail = (props) => {
  const [visible, setVisible] = useState(false);

  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {
      setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const openModal = () => {
    setVisible(true);
  };

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Manage Orders</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-orders/order">Order Detail</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Order Detail #100001"
              className="site-page-header"
              extra={[
                <Button key="2" className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
            <hr />
            <Modal
                visible={visible}
                title="Change Order Status!"
                onOk={handleOk}
                onCancel={handleCancel}
                footer={[
                  <Button key="back" onClick={handleCancel}>
                    Return
                  </Button>,
                  <Button key="submit" type="primary" onClick={handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <Form
                  layout="vertical"
                  name="productForm"
                >
                  <Form.Item label="Update Order Status">
                                <Form.Item name="brand_id" noStyle>
                                <Select
                                  showSearch
                                  style={{ width: 200 }}
                                  placeholder="Select"
                                  optionFilterProp="children"
                                  filterOption={(input, option) =>
                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                  }
                                >
                                  <Option value="Pending">Pending</Option>
                                  <Option value="Shipped">Shipped</Option>
                                  <Option value="Delivered">Delivered</Option>
                                  <Option value="Canceled">Canceled</Option>
                                </Select>
                                </Form.Item>
                  </Form.Item>
                </Form>
              </Modal>
            <div className="row col-md-12 mt-3">
                <div className="col-md-8">
                    <b className="item-count">Total No. of Items: 3</b><br /><br />
                <div className="item-sec-box mb-2">
                    <div className="item-section">
                        <p className="order-item-box prod-no">Product No: #123</p>
                        <p className="order-item-box prod-name">5 Pills of Modafinil, <span className="qty-label">3 qty</span></p>
                    </div>
                    <div className="my-auto order-total-amount">
                        Rs. 547.03
                    </div>
                </div>
                <div className="item-sec-box mb-2">
                    <div className="item-section">
                        <p className="order-item-box prod-no">Product No: #101</p>
                        <p className="order-item-box prod-name">Acetaminophen Box, <span className="qty-label">1 qty</span></p>
                    </div>
                    <div className="my-auto order-total-amount">
                    Rs. 200.00
                    </div>
                </div>
                <div className="item-sec-box mb-2">
                    <div className="item-section">
                        <p className="order-item-box prod-no">Product No: #123</p>
                        <p className="order-item-box prod-name">5 Pills of Modafinil, <span className="qty-label">3 qty</span></p>
                    </div>
                    <div className="my-auto order-total-amount">
                        Rs. 547.03
                    </div>
                </div>
                <div className="col-md-6 float-right">
                <div className="order-transaction mt-3">
                <p><b>Sub Total: </b> <span className="float-right">Rs. 800</span></p>
                <p><b>Discount: </b> <span className="float-right">Rs. 100</span></p>
                <p><b>Delivery Charge: </b> <span className="float-right">Rs. 50</span></p>
                <hr />
                <p><b>Total Payable Amount: </b> <span className="float-right">Rs. 750</span></p>
                </div>
                </div>
                </div>
                <div className="col-md-4">
                    <p><b>Order Placed: </b> 2020-03-09</p>
                    <p><b>Order Status: </b> Deliverd 
                    <span className="edit-order-status ml-2">
                        <a onClick={openModal}><i className="fa fa-edit"></i></a>
                    </span></p>
                    <p><b>For User: </b> Michael Johnson</p>
                <div className="tab-section spacing">
                <Tabs
                  defaultActiveKey="1"
                  type="card"
                  className="order"
                >
                  <TabPane tab="Billing Address" key="1">
                    <div className="tab-content order">
                      <div className="tab-inner-content order-detail">
                        <div className="order-address">
                            <div className="order-address-title">
                                State:
                            </div>
                            <div className="order-address-detail">
                                Central
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                City:
                            </div>
                            <div className="order-address-detail">
                                New Baneshwor, Kathmandu
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                Street:
                            </div>
                            <div className="order-address-detail">
                                Radha Marga, Shankhamul
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                Zip Code:
                            </div>
                            <div className="order-address-detail">
                                44600
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                Phone:
                            </div>
                            <div className="order-address-detail">
                                +977 9811050526
                            </div>
                        </div>
                      </div>
                    </div>
                  </TabPane>
                  <TabPane tab="Shipping Address" key="2">
                  <div className="tab-content order">
                      <div className="tab-inner-content order-detail">
                        <div className="order-address">
                            <div className="order-address-title">
                                State:
                            </div>
                            <div className="order-address-detail">
                                Central
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                City:
                            </div>
                            <div className="order-address-detail">
                                New Baneshwor, Kathmandu
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                Street:
                            </div>
                            <div className="order-address-detail">
                                Radha Marga, Shankhamul
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                Zip Code:
                            </div>
                            <div className="order-address-detail">
                                44600
                            </div>
                        </div>
                        <div className="order-address">
                            <div className="order-address-title">
                                Phone:
                            </div>
                            <div className="order-address-detail">
                                +977 9811050526
                            </div>
                        </div>
                      </div>
                    </div>
                  </TabPane>
                </Tabs>
              </div>
                </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default OrderDetail;
