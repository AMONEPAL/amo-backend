import React, { useEffect } from "react";
import { connect } from 'react-redux'
import { Modal, Card, Avatar } from "antd";

const CustomerDetail = ({visible, onCancel}) => {
    return (
        <div>
            <Modal
            visible={visible}
            title="Customer Detail"
            footer={null}
            onCancel={onCancel}
            width={600}
            style={{ top: 25 }}
            >
                <h5 className="tab-title">GENERAL INFORMATION</h5>
                <hr className="hrmodal"/>
                <div className="row display-flex">
                <div className="col modal-desc">
                    <p>Full Name: Manoz Acharya</p>
                    <p>Username: acharyamanoz</p>
                    <p>Contact: 9811050526</p>
                    <p>Email: manozacharya1998@gmail.com</p>
                </div>
                <div className="col moal-profile-pic">
                <Avatar size={125} src="https://findyourmomtribe.com/wp-content/uploads/2020/04/Cute-baby-smiling-.jpg" className="float-right" />
                </div>
                </div>

                <h5 className="tab-title mt-4">ADDRESS DETAIL</h5>
                <hr className="hrmodal"/>
                <div className="row display-flex">
                    <div className="col modal-desc">
                    <p className="modal-inner-sub-title">BILLING</p>
                    <Card className="card-billing">
                        <p><span className="inner-strong">State:</span> 03</p>
                        <p>City: Banaeshwor</p>
                        <p>Street: Shankhamul</p>
                        <p>Zip Code: 44600</p>
                        <p>Phone: 9811050526</p>
                        </Card>
                    </div>
                    <div className="col modal-desc">
                    <p className="modal-inner-sub-title">SHIPPING</p>
                        <Card className="card-shipping">
                        <p>State: 03</p>
                        <p>City: Banaeshwor</p>
                        <p>Street: Shankhamul</p>
                        <p>Zip Code: 44600</p>
                        <p>Phone: 9811050526</p>
                        </Card>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

const mapStateToProps = (state) => ({
    
})

export default connect(mapStateToProps, null)(CustomerDetail)
