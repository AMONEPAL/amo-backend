import React, {useEffect, useState } from 'react'
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { connect } from 'react-redux'
import {
    Layout,
    Breadcrumb,
    Table,
    Tag,
    PageHeader,
    Menu,
    Spin,
    Popconfirm,
    Button,
    Dropdown,
  } from "antd";
import { Link } from "react-router-dom";
import CustomerDetail from './CustomerDetail';

const { Content } = Layout;

const Customers = (props) => {
    const [tableData, setTableData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [customerID, setCustomerID] = useState(null);
    const [customerViewClicked, setCustomerViewClicked] = useState(false);
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        setLoading(false);
          let data = [
            {
                key: '1',
                name: 'ATM Pharmacy',
                username: 'atmadmin',
                contact: 9811050526,
                email: 'atm@atm.com'
              },
              {
                key: '2',
                name: 'ATM Pharmacy',
                username: 'atmadmin',
                contact: 9811050526,
                email: 'atm@atm.com'
              },
          ];
          setTableData(data);
      }, []);

    const exportoption = (
        <Menu>
          <Menu.Item>
            <a href="#">PRINT</a>
          </Menu.Item>
          <Menu.Item>
            <a href="#">EXCEL</a>
          </Menu.Item>
          <Menu.Item>
            <a href="#">PDF</a>
          </Menu.Item>
        </Menu>
      );

      const columns = [
        {
          title: "Full Name",
          dataIndex: "name",
          key: "name",
          width: 250,
        },
        {
          title: "Username",
          dataIndex: "username",
          key: "username",
        },
        {
          title: "Contact",
          dataIndex: "contact",
          key: "contact",
        },
        {
          title: "Email",
          dataIndex: "email",
          key: "email",
          },
        {
          title: "Action",
          key: "action",
          render: (text, record) => (
            <>
              <span>
                <a
                  onClick={() => {
                    openCustomerModal(record.key);
                  }}
                >
                  <i className="fa fa-eye ml-3"></i>
                </a>
              </span>
            </>
          ),
        },
      ];

      const openCustomerModal = (val) => {
        setVisible(true);
        setCustomerID(val);
      };

    return (
        <>
        <Layout>
            <SideNav className="side-nav-fixed" />
            <Layout className="site-layout">
            <TopHeader />
            <Content
                className="site-layout-background"
            >
                <Breadcrumb className="admin-breadcrumb">
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to="/customers">Customers List</Link>
                </Breadcrumb.Item>
                </Breadcrumb>
                <PageHeader
                title="Customers List"
                className="site-page-header"
                extra={[
                    <Button key="2" className="extra-btn">
                      <Dropdown overlay={exportoption}>
                        <a
                          className="ant-dropdown-link"
                          onClick={(e) => e.preventDefault()}
                        >
                          Export <i className="fas fa-caret-down"></i>
                        </a>
                      </Dropdown>
                    </Button>,
                  ]}
                ></PageHeader>
                <div className="table-section spacing">
                    <div className="table-responsive">
                        {loading ? (
                        <Spin>
                            <Table
                            bordered={true}
                            columns={columns}
                            dataSource={tableData}
                            />
                        </Spin>
                        ) : (
                        <Table
                            bordered={true}
                            columns={columns}
                            dataSource={tableData}
                            pagination={{ pageSize: 10 }}
                        />
                        )}
                    </div>
                    </div>
                    <CustomerDetail
                      visible={visible}
                      onCancel={() => {
                        setVisible(false);
                      }}
                    />
            </Content>
            </Layout>
        </Layout>
    </>
    )
}

const mapStateToProps = (state) => ({
    
})

export default connect(mapStateToProps, null)(Customers)