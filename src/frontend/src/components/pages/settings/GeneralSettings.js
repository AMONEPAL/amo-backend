import React, {useEffect, useState } from 'react'
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { connect } from 'react-redux'
import {
    Layout,
    Breadcrumb,
    Table,
    Tag,
    PageHeader,
    Menu,
    Spin,
    Popconfirm,
    Button,
    Dropdown,
  } from "antd";
import { Link } from "react-router-dom";

const { Content } = Layout;

const GeneralSettings = (props) => {
        return (
            <>
            <Layout>
                <SideNav className="side-nav-fixed" />
                <Layout className="site-layout">
                <TopHeader />
                <Content
                    className="site-layout-background"
                >
                    <Breadcrumb className="admin-breadcrumb">
                    <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <Link to="/general-settings">General Settings</Link>
                    </Breadcrumb.Item>
                    </Breadcrumb>
                    <PageHeader
                    title="General Setting"
                    className="site-page-header"
                    ></PageHeader>
                  
                </Content>
                </Layout>
            </Layout>
        </>
        )
}

const mapStateToProps = (state) => ({
    
})

export default connect(mapStateToProps, null)(GeneralSettings)