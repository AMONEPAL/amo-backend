import React, {useEffect, useState } from 'react'
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { connect } from 'react-redux'
import {
    Layout,
    Breadcrumb,
    Table,
    Tag,
    PageHeader,
    Menu,
    Spin,
    Popconfirm,
    Button,
    Dropdown,
    Collapse,
    Form,
    Input,
  } from "antd";
import { Link } from "react-router-dom";

const { Content } = Layout;
const { Panel } = Collapse;

const MetaDescription = (props) => {
  const [form] = Form.useForm();
        return (
            <>
            <Layout>
                <SideNav className="side-nav-fixed" />
                <Layout className="site-layout">
                <TopHeader />
                <Content
                    className="site-layout-background"
                >
                    <Breadcrumb className="admin-breadcrumb">
                    <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <Link to="/coupons">Meta Tags and Description</Link>
                    </Breadcrumb.Item>
                    </Breadcrumb>
                    <PageHeader
                    title="Meta Tags and Description"
                    className="site-page-header"
                    ></PageHeader>
                    <div className="meta-desc-page-content">
                      <div className="row">
                        <div className="col-md-8 mx-auto meta-note">
                          <p>Always remember that Meta tags and their descriptions are written for the search engines to understand the content on a page.
                            It helps your website to appear in search engine results with good ranking, 
                            relevancy and with overtime your website gains the authority in the related domain.</p>
                        </div>
                      </div>
                      <div className="meta-tabs mt-4">
                      <Collapse bordered={false} accordion>
                        <Panel header="Home Page" key="1">
                          <div className="collapse-content">
                          <Form
                            layout="vertical"
                            form={form}
                            className="collapse-form"
                          >
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                              >
                                <Form.Item
                                 label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Maximum 255 characters title are preffered for the best SEO practises.</span>
                                </Form.Item>
                            </Form.Item>
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Keywords <span className="small-text-label">(Comma Seprated)</span>
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Provide mix of long-tail and short-tail keywords relevant to the page content.</span>
                                </Form.Item>
                                </Form.Item>
                                <Form.Item
                                label={
                                  <b className="label-bold">
                                    Meta Description
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input.TextArea />
                                </Form.Item>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="save-btn">
                                  SAVE
                                </Button>
                              </Form.Item>
                          </Form>
                          </div>
                        </Panel>
                        <Panel header="Products Page" key="2">
                        <div className="collapse-content">
                          <Form
                            layout="vertical"
                            form={form}
                            className="collapse-form"
                          >
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                              >
                                <Form.Item
                                 label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Maximum 255 characters title are preffered for the best SEO practises.</span>
                                </Form.Item>
                            </Form.Item>
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Keywords <span className="small-text-label">(Comma Seprated)</span>
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Provide mix of long-tail and short-tail keywords relevant to the page content.</span>
                                </Form.Item>
                                </Form.Item>
                                <Form.Item
                                label={
                                  <b className="label-bold">
                                    Meta Description
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input.TextArea />
                                </Form.Item>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="save-btn">
                                  SAVE
                                </Button>
                              </Form.Item>
                          </Form>
                          </div>
                        </Panel>
                        <Panel header="About Page" key="3">
                        <div className="collapse-content">
                          <Form
                            layout="vertical"
                            form={form}
                            className="collapse-form"
                          >
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                              >
                                <Form.Item
                                 label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Maximum 255 characters title are preffered for the best SEO practises.</span>
                                </Form.Item>
                            </Form.Item>
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Keywords <span className="small-text-label">(Comma Seprated)</span>
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Provide mix of long-tail and short-tail keywords relevant to the page content.</span>
                                </Form.Item>
                                </Form.Item>
                                <Form.Item
                                label={
                                  <b className="label-bold">
                                    Meta Description
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input.TextArea />
                                </Form.Item>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="save-btn">
                                  SAVE
                                </Button>
                              </Form.Item>
                          </Form>
                          </div>
                        </Panel>
                        <Panel header="Contact Page" key="4">
                        <div className="collapse-content">
                          <Form
                            layout="vertical"
                            form={form}
                            className="collapse-form"
                          >
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                              >
                                <Form.Item
                                 label={
                                  <b className="label-bold">
                                    Page Title
                                  </b>
                                }
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Maximum 255 characters title are preffered for the best SEO practises.</span>
                                </Form.Item>
                            </Form.Item>
                            <Form.Item
                                label={
                                  <b className="label-bold">
                                    Keywords <span className="small-text-label">(Comma Seprated)</span>
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input />
                                  <span className="small-text-label">Provide mix of long-tail and short-tail keywords relevant to the page content.</span>
                                </Form.Item>
                                </Form.Item>
                                <Form.Item
                                label={
                                  <b className="label-bold">
                                    Meta Description
                                  </b>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  noStyle
                                >
                                  <Input.TextArea />
                                </Form.Item>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="save-btn">
                                  SAVE
                                </Button>
                              </Form.Item>
                          </Form>
                          </div>
                        </Panel>
                      </Collapse>
                      </div>
                    </div>
                </Content>
                </Layout>
            </Layout>
        </>
        )
}

const mapStateToProps = (state) => ({
    
})

export default connect(mapStateToProps, null)(MetaDescription)