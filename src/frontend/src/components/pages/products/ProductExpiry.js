import React, { Fragment, useEffect, useState } from "react";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  PageHeader,
  Menu,
  Spin,
  Popconfirm,
} from "antd";
import { Redirect } from "react-router-dom";
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getProducts, deleteProduct } from "../../../actions/product";
import moment from "moment";

const { Content } = Layout;

const ProductExpiry = (props) => {
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [productID, setProductID] = useState(null);
  const [productViewClicked, setProductViewClicked] = useState(false);

  useEffect(() => {
    props.getProducts();
  }, []);

  useEffect(() => {
    if (props.products != null) {
      setLoading(false);
      let data = [];
      props.products.map((element) => {
        data.push({
          key: element.id,
          name: element.title,
          expiry_date: element.expiry_date,
          batch_number: element.batch_number,
          status: moment([
            moment(element.expiry_date).get("year"),
            moment(element.expiry_date).get("month"),
            moment(element.expiry_date).get("date"),
          ]).diff(
            moment([
              moment().get("year"),
              moment().get("month"),
              moment().get("date"),
            ]),
            "days"
          ),
        });
      });
      setTableData(data);
    }
  }, [props.products]);

  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: 250,
    },
    {
      title: "Batch No.",
      dataIndex: "batch_number",
      key: "batch_number",
    },
    {
      title: "Expiry Date",
      dataIndex: "expiry_date",
      key: "expiry_date",
      sorter: (a, b) => moment(a.expiry_date) - moment(b.expiry_date),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      filters: [
        {
          text: "Expiring in 60 days",
          value: 60,
        },
        {
          text: "Expiring in 30 days",
          value: 30,
        },
        {
          text: "Expired",
          value: 0,
        },
      ],
      onFilter: (value, record) => {
        if (record.status <= value && record.status > 0) {
          return true;
        } else if (record.status <= 0) {
          return true;
        }
      },
      filterMultiple: false,
      render: (days) => {
        if (days <= 60 && days > 0) {
          if (days <= 30) {
            return <Tag color="orange">Expires in {days} days</Tag>;
          } else {
            return <Tag color="gold">Expires in {days} days</Tag>;
          }
        } else if (days <= 0) {
          return <Tag color="red">Expired</Tag>;
        } else {
          let month = Math.floor(days / 30);
          return <Tag color="green">Expires after {month} months</Tag>;
        }
      },
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Fragment>
          <span>
            <a
              onClick={() => {
                handleProductView(record.key);
              }}
            >
              <i className="fa fa-eye ml-3"></i>
            </a>
          </span>
          <span style={{ paddingLeft: "10px" }}>
            <a className="delete-popover">
              <Popconfirm
                title="Are you sure want to delete this product?"
                onConfirm={() => handleDeleteProduct(record.key)}
                okText="Yes"
                cancelText="No"
              >
                <i className="fa fa-trash delete-icon"> </i>
              </Popconfirm>
            </a>
          </span>
        </Fragment>
      ),
    },
  ];

  if (productViewClicked) {
    return <Redirect to={"/manage-product/detail/" + productID} />;
  }

  const handleProductView = (id) => {
    setProductID(id);
    setProductViewClicked(true);
  };

  const handleDeleteProduct = (e) => {
    props.deleteProduct(e);
  };

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
              <Breadcrumb.Item>Expiry Products</Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Product Expiry Details"
              className="site-page-header"
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="table-section spacing">
              <div className="table-responsive">
                {loading ? (
                  <Spin>
                    <Table
                      bordered={true}
                      columns={columns}
                      dataSource={tableData}
                    />
                  </Spin>
                ) : (
                  <Table
                    bordered={true}
                    columns={columns}
                    dataSource={tableData}
                    pagination={{ pageSize: 10 }}
                  />
                )}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

const mapStateToProps = (state) => ({
  products: state.product.products,
});

export default connect(mapStateToProps, { getProducts, deleteProduct })(
  ProductExpiry
);
