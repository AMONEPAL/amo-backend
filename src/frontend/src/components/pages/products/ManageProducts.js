import React, { Fragment, useEffect, useState } from "react";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  Button,
  Space,
  PageHeader,
  Dropdown,
  Menu,
  Spin,
} from "antd";
import { Redirect } from "react-router-dom";
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getProducts } from "../../../actions/product";
import ManageProductVariation from "./ManageProductVariation";

const { Content } = Layout;

const ManageProducts = (props) => {
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [productID, setProductID] = useState(null);
  const [productVariationClicked, setProductVariationClicked] = useState(false);

  useEffect(() => {
    props.getProducts();
  }, []);

  useEffect(() => {
    if (props.products != null) {
      setLoading(false);
      let data = [];
      props.products.map((element) => {
        data.push({
          key: element.id,
          name: element.title,
          generic_name: "Oxymoron",
          category: "Pain Relief",
          created: "2020-07-19",
          quantity: 77,
          cost_price: element.price,
          sell_price: 4,
          available: 56,
          status: ["active"],
        });
      });
      setTableData(data);
    }
  }, [props.products]);

  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const handleProductVariation = (id) => {
    setProductID(id);
    setProductVariationClicked(true);
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: 250,
    },
    {
      title: "Generic Name",
      dataIndex: "generic_name",
      key: "generic_name",
      width: 150,
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Created",
      dataIndex: "created",
      key: "created",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      title: "Cost Price",
      dataIndex: "cost_price",
      key: "cost_price",
    },
    {
      title: "Sell Price",
      dataIndex: "sell_price",
      key: "sell_price",
    },
    {
      title: "Available",
      dataIndex: "available",
      key: "available",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (tags) => (
        <>
          {tags.map((tag) => {
            let color;
            if (tag === "active") {
              color = "green";
            }
            if (tag === "draft") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Fragment>
          <span>
            <a
              onClick={() => {
                handleProductVariation(record.key);
              }}
            >
              <i className="fa fa-plus plus-icon"></i>
            </a>
          </span>
        </Fragment>
      ),
    },
  ];

  if (productVariationClicked) {
    return <Redirect to={"/manage-product/variation/" + productID} />;
  }

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Products</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-products">Manage Products</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Product Overview"
              className="site-page-header"
              extra={[
                <Button key="3" type="primary" className="save-btn">
                  <a href="#/manage-products/add" className="btn-link">
                  <i className="fas fa-plus mr-2"></i>
                    Add New
                  </a>
                </Button>,
                <Button key="2" className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="table-section spacing">
              <div className="table-responsive">
                {loading ? (
                  <Spin>
                    <Table
                      bordered={true}
                      columns={columns}
                      dataSource={tableData}
                    />
                  </Spin>
                ) : (
                  <Table
                    bordered={true}
                    columns={columns}
                    dataSource={tableData}
                  />
                )}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

const mapStateToProps = (state) => ({
  products: state.product.products,
});

export default connect(mapStateToProps, { getProducts })(ManageProducts);
