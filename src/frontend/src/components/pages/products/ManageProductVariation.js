import React, { Fragment, useEffect, useState } from "react";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  Button,
  PageHeader,
  Dropdown,
  Menu,
  Spin,
  Form,
  Popconfirm,
} from "antd";
import { Redirect } from "react-router-dom";
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  getProductVariations,
  addProductVariation,
  updateProductVariation,
} from "../../../actions/product_variation";
import { getProductById } from "../../../actions/product";

import ProductVariationForm from "./ProductVariationForm";
import EditableCell from "../../product/EditableCell";

const { Content } = Layout;

const ManageProductVariation = (props) => {
  const [form] = Form.useForm();
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [recordFound, setRecordFound] = useState(true);
  const [productTitle, setProductTitle] = useState("Product Variation");

  const [visible, setVisible] = useState(false);

  const [editingKey, setEditingKey] = useState("");
  const isEditing = (record) => record.key === editingKey;

  useEffect(() => {
    props.getProductVariations(props.computedMatch.params.productID);
    props.getProductById(props.computedMatch.params.productID);
  }, []);

  useEffect(() => {
    if (props.product_variations != null) {
      setLoading(false);
      props.count == 0 && setRecordFound(false);
      let data = [];
      props.product_variations.map((element) => {
        data.push({
          key: element.id,
          name: element.title,
          price: element.price,
          sale_price: element.sale_price,
          inventory: element.inventory,
          status: ["active"],
        });
      });
      setTableData(data);
    }
  }, [props.product_variations]);

  useEffect(() => {
    if (props.product != null) {
      setProductTitle(`Product Variation - ${props.product.title}`);
    }
  }, [props.product]);

  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  //Table Function --START--////////////////////////

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: 150,
      editable: true,
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
      editable: true,
    },
    {
      title: "Sale Price",
      dataIndex: "sale_price",
      key: "sale_price",
      editable: true,
    },
    {
      title: "Inventory",
      dataIndex: "inventory",
      key: "inventory",
      editable: true,
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (tags) => (
        <>
          {tags.map((tag) => {
            let color;
            if (tag === "active") {
              color = "green";
            }
            if (tag === "draft") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Button
              type="primary"
              className="save-btn"
              onClick={() => saveRow(record.key)}
            >
              Save
            </Button>
            <Button className="extra-btn ml-3" onClick={cancelEdit}>
              Cancel
            </Button>
          </span>
        ) : (
          <Fragment>
            <span>
              <a
                disabled={editingKey !== ""}
                onClick={() => {
                  editRow(record);
                }}
                style={{
                  marginRight: 8,
                  color: "#3a4991",
                }}
              >
                <i className="fa fa-edit edit-icon"></i>
              </a>

              <a
                disabled={editingKey !== ""}
                onClick={() => {
                  console.log("delete");
                }}
                style={{
                  color: "#cc101c",
                }}
              >
                <i className="fa fa-trash"></i>
              </a>
            </span>
          </Fragment>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => {
    if (!col.editable) {
      return col;
    }
    return {
      ...col,
      onCell: (record) => ({
        record,
        inputType: col.dataIndex === "name" ? "text" : "number",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  const editRow = (record) => {
    form.setFieldsValue({
      title: "",
      price: "",
      sale_price: "",
      inventory: "",
      ...record,
    });
    setEditingKey(record.key);
  };

  const cancelEdit = () => {
    setEditingKey("");
  };

  const saveRow = async (key) => {
    try {
      const row = await form.validateFields();
      const newData = [...tableData];
      const index = newData.findIndex((item) => key === item.key);

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, { ...item, ...row });

        setTableData(newData);
        setEditingKey("");

        const data = {
          variation_id: item.key,
          title: row.name,
          price: row.price,
          sale_price: row.sale_price,
          inventory: row.inventory,
          active: true,
        };
        props.updateProductVariation(data);
      } else {
        newData.push(row);
        setTableData(newData);
        setEditingKey("");
      }
    } catch (errInfo) {
      console.log("Validate Failed:", errInfo);
    }
  };

  //Table Function --END--////////////////////////

  const handleCreate = (val) => {
    console.log(val);
    const data = {
      title: val.title,
      price: val.price,
      sale_price: val.sale_price,
      inventory: val.inventory,
      active: true,
      product_id: props.match.params.productID,
    };

    props.addProductVariation(data);
  };

  if (!recordFound) {
    return <Redirect to="/manage-products" />;
  }

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Products</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-products">Manage Products</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>Product Variations</Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title={productTitle}
              className="site-page-header"
              extra={[
                <Button key="3" type="primary" className="save-btn">
                  <a onClick={() => setVisible(true)} className="btn-link">
                    <i className="fas fa-plus mr-2"></i>
                    Add New
                  </a>
                </Button>,
                <Button key="2" className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="table-section spacing">
              <div className="table-responsive">
                {loading ? (
                  <Spin>
                    <Table
                      bordered={true}
                      columns={columns}
                      dataSource={tableData}
                    />
                  </Spin>
                ) : (
                  <Form form={form} component={false}>
                    <Table
                      components={{
                        body: {
                          cell: EditableCell,
                        },
                      }}
                      bordered={true}
                      columns={mergedColumns}
                      rowClassName="editable-row"
                      dataSource={tableData}
                      pagination={{
                        onChange: cancelEdit,
                      }}
                    />
                  </Form>
                )}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
      <ProductVariationForm
        visible={visible}
        onCancel={() => setVisible(false)}
        onCreate={handleCreate}
      />
    </>
  );
};

const mapStateToProps = (state) => ({
  product: state.product.product,
  product_variations: state.product_variation.product_variations,
  count: state.product_variation.count,
});

export default connect(mapStateToProps, {
  getProductById,
  getProductVariations,
  addProductVariation,
  updateProductVariation,
})(ManageProductVariation);
