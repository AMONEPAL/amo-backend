import React, { useEffect, useState } from "react";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { connect } from "react-redux";
import { getBrands } from "../../../actions/brand";
import { getCategories } from "../../../actions/category";
import { getGenericNames } from "../../../actions/generic_name";
import { getProductUnits } from "../../../actions/product_unit";
import { getCompanies } from "../../../actions/company";
import { addProduct } from "../../../actions/product";
import {
  Layout,
  Breadcrumb,
  Button,
  Select,
  PageHeader,
  Tabs,
  Form,
  Input,
  Upload,
  DatePicker,
  message,
} from "antd";
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { Link } from "react-router-dom";
import { UploadOutlined } from "@ant-design/icons";
import AnchorLink from "react-anchor-link-smooth-scroll";

const { Content } = Layout;
const { TabPane } = Tabs;
const { Option } = Select;

const renderTabBar = (props, DefaultTabBar) => (
  <Sticky bottomOffset={80}>
    {({ style }) => (
      <DefaultTabBar
        {...props}
        className="site-custom-tab-bar"
        style={{ ...style }}
      />
    )}
  </Sticky>
);

const AddProduct = (props) => {
  const [form] = Form.useForm();
  const [categorySelect, setCategorySelect] = useState(null);
  const [brandSelect, setBrandSelect] = useState(null);
  const [genericNameSelect, setGenericNameSelect] = useState(null);
  const [productUnitSelect, setProductUnitSelect] = useState(null);
  const [companySelect, setCompanySelect] = useState(null);
  const [expDate, setExpDate] = useState("");
  const [description, setDescription] = useState("");
  const [activeTabFirst, setActiveTabFirst] = useState(true);
  const [activeTabSecond, setActiveTabSecond] = useState(false);
  const [activeTabThird, setActiveTabThird] = useState(false);

  useEffect(() => {
    props.getCategories();
    props.getBrands();
    props.getGenericNames();
    props.getProductUnits();
    props.getCompanies();
  }, []);

  useEffect(() => {
    props.categories != null &&
      setCategorySelect(
        props.categories.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
  }, [props.categories]);

  useEffect(() => {
    props.brands != null &&
      setBrandSelect(
        props.brands.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
  }, [props.brands]);

  useEffect(() => {
    props.product_unit != null &&
      setProductUnitSelect(
        props.product_unit.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
  }, [props.product_unit]);

  useEffect(() => {
    props.generic_names != null &&
      setGenericNameSelect(
        props.generic_names.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
  }, [props.generic_names]);

  useEffect(() => {
    props.companies != null &&
      setCompanySelect(
        props.companies.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
  }, [props.companies]);

  const photoProps = {
    action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    listType: "picture",

    onChange(info) {
      if (info.file.status !== "uploading") {
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    progress: {
      strokeColor: {
        "0%": "#108ee9",
        "100%": "#87d068",
      },
      strokeWidth: 3,
      format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
    },
  };

  const onDateChange = (date, dateString) => {
    setExpDate(dateString);
  };

  const handleDescription = (data) => {
    setDescription(data);
  };

  const onFinish = (val) => {
    let form_data = new FormData();
    let files = [];

    try {
      files = val.file.fileList;
    } catch (error) {}

    files.length > 0
      ? files.map((element, i) => {
          if (element.status === "done") {
            form_data.append("file", element.originFileObj);
          }
        })
      : form_data.append("file", "");

    let data = {
      product_title: val.product_title,
      price: val.price,
      description: description || "",
      category_id: val.category_id || "",
      brand_id: val.brand_id || "",
      product_unit_id: val.product_unit_id || "",
      generic_names_id: val.generic_names_id || "",
      company_id: val.company_id || "",
      batch_number: val.batch_number || "",
      expiry_date: expDate,
    };

    Object.keys(data).map((key) => form_data.append(key, data[key]));

    props.addProduct(form_data);
    // form.resetFields();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const handleFirstTab = () => {
    setActiveTabFirst(true);
    setActiveTabSecond(false);
    setActiveTabThird(false);
  };

  const handleSecondTab = () => {
    setActiveTabFirst(false);
    setActiveTabSecond(true);
    setActiveTabThird(false);
  };

  const handleThirdTab = () => {
    setActiveTabFirst(false);
    setActiveTabSecond(false);
    setActiveTabThird(true);
  };

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Products</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-products">Manage Products</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-products/add">Add Product</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Add Product"
              className="site-page-header"
              extra={[
                <Button key="1" className="extra-btn">
                  Import
                </Button>,
              ]}
            ></PageHeader>
            <Form
              layout="vertical"
              form={form}
              name="productForm"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              <div>
                <div id="sticky-scroll">
                  <ul className="add-tabs">
                    <li>
                      <AnchorLink
                        href="#basic-information"
                        className={activeTabFirst ? "active-tab" : ""}
                        onClick={handleFirstTab}
                      >
                        Basic Information
                      </AnchorLink>
                    </li>
                    <li>
                      <AnchorLink
                        href="#image-and-description"
                        className={activeTabSecond ? "active-tab" : ""}
                        onClick={handleSecondTab}
                      >
                        Image & Description
                      </AnchorLink>
                    </li>
                    <li>
                      <AnchorLink
                        href="#price-and-stock"
                        className={activeTabThird ? "active-tab" : ""}
                        onClick={handleThirdTab}
                      >
                        Price & Stock
                      </AnchorLink>
                    </li>
                  </ul>
                </div>

                <div id="basic-information">
                  <div className="tab-content mt-3">
                    <h5 className="tab-title">Basic Information</h5>
                    <div className="tab-inner-content">
                      <div className="row">
                        <div className="col-md-6">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }} className="mr-1">
                                  *
                                </span>
                                Product Name
                              </div>
                            }
                          >
                            <Form.Item
                              name="product_title"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input title!",
                                },
                              ]}
                              noStyle
                            >
                              <Input placeholder="Ex. Acetaminophen 1 Box (Contains 100 pcs.)" />
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-5 ml-3 my-auto convention">
                          <i>
                            * Write User-Friendly & Search Engine Optimized
                            Product Name.
                          </i>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-5">
                          <Form.Item label="Generic Name">
                            <Form.Item name="generic_name_id" noStyle>
                              <Select
                                showSearch
                                optionFilterProp="children"
                                placeholder="Select Generic Name"
                              >
                                {genericNameSelect}
                              </Select>
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-3">
                          <Form.Item label="Category">
                            <Form.Item name="category_id" noStyle>
                              <Select
                                showSearch
                                optionFilterProp="children"
                                placeholder="Select Category"
                              >
                                {categorySelect}
                              </Select>
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-3">
                          <Form.Item label="Brand">
                            <Form.Item name="brand_id" noStyle>
                              <Select
                                showSearch
                                optionFilterProp="children"
                                placeholder="Select Brand"
                              >
                                {brandSelect}
                              </Select>
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-3">
                          <Form.Item label="Company">
                            <Form.Item name="company_id" noStyle>
                              <Select
                                showSearch
                                optionFilterProp="children"
                                placeholder="Select Company"
                              >
                                {companySelect}
                              </Select>
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-2">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }} className="mr-1">
                                  *
                                </span>
                                Batch No.
                              </div>
                            }
                          >
                            <Form.Item
                              name="batch_number"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input title!",
                                },
                              ]}
                              noStyle
                            >
                              <Input />
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-3">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }} className="mr-1">
                                  *
                                </span>
                                Expiry Date
                              </div>
                            }
                          >
                            <Form.Item
                              name="expiry_date"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input Expiry Date!",
                                },
                              ]}
                              noStyle
                            >
                              <DatePicker
                                onChange={onDateChange}
                                style={{ width: 200 }}
                              />
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="image-and-description">
                  <div className="tab-content">
                    <h5 className="tab-title">Images & Description</h5>
                    <div className="tab-inner-content">
                      <div className="row">
                        <div className="col-md-5">
                          <Form.Item label="Image Upload">
                            <Form.Item name="file" noStyle>
                              <Upload {...photoProps}>
                                <Button>
                                  <UploadOutlined /> Select Images(s)
                                </Button>
                              </Upload>
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-6 ml-3 my-auto convention">
                          <i>
                            * Multiple images can be uploaded. Size between 500
                            x 500 and 1500 x 1500 are recommended.
                          </i>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-10">
                          <Form.Item label=" Product Description">
                            <Form.Item name="description" noStyle>
                              <CKEditor
                                editor={ClassicEditor}
                                // data=""
                                onInit={(editor) => {}}
                                onChange={(event, editor) => {
                                  const data = editor.getData();
                                  // console.log({ event, editor, data });
                                  handleDescription(data);
                                }}
                                onBlur={(event, editor) => {
                                  // console.log("Blur.", editor);
                                }}
                                onFocus={(event, editor) => {
                                  // console.log("Focus.", editor);
                                }}
                              />
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div id="price-and-stock">
                  <div className="tab-content last-sec">
                    <h5 className="tab-title">Price & Stock</h5>
                    <div className="tab-inner-content">
                      <div className="row">
                        <div className="col-md-3">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }}>*</span>
                                &nbsp;Cost Price
                              </div>
                            }
                          >
                            <Form.Item
                              name="price"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input price!",
                                },
                              ]}
                              noStyle
                            >
                              <Input />
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-3">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }}>*</span>
                                &nbsp;Selling Price
                              </div>
                            }
                          >
                            <Form.Item
                              name="selling_price"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input price!",
                                },
                              ]}
                              noStyle
                            >
                              <Input />
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-4">
                          <Form.Item label="Product Unit">
                            <Form.Item name="product_unit_id" noStyle>
                              <Select
                                showSearch
                                optionFilterProp="children"
                                placeholder="Select Product Unit"
                              >
                                {productUnitSelect}
                              </Select>
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* <StickyContainer>
                <div className="tab-section spacing">
                  <Tabs
                    defaultActiveKey="1"
                    type="card"
                    renderTabBar={renderTabBar}
                  >
                    <TabPane tab="Basic Information" key="1">
                      <div className="tab-content">
                        <h5 className="tab-title">Basic Information</h5>
                        <div className="tab-inner-content">
                        <div className="row">
                        <div className="col-md-6">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Product Name
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="product_title"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input title!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input placeholder="Ex. Acetaminophen 1 Box (Contains 100 pcs.)"/>
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-5 ml-3 my-auto convention">
                            <i>
                              * Write User-Friendly & Search Engine Optimized Product Name.
                            </i>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-5">
                              <Form.Item label="Generic Name">
                                <Form.Item name="generic_name_id" noStyle>
                                  <Select
                                    showSearch
                                    optionFilterProp="children"
                                    placeholder="Select Generic Name"
                                  >
                                    {genericNameSelect}
                                  </Select>
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-3">
                              <Form.Item label="Category">
                                <Form.Item name="category_id" noStyle>
                                  <Select
                                    showSearch
                                    optionFilterProp="children"
                                    placeholder="Select Category"
                                  >
                                    {categorySelect}
                                  </Select>
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-3">
                              <Form.Item label="Brand">
                                <Form.Item name="brand_id" noStyle>
                                  <Select
                                    showSearch
                                    optionFilterProp="children"
                                    placeholder="Select Brand"
                                  >
                                    {brandSelect}
                                  </Select>
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-3">
                              <Form.Item label="Company">
                                <Form.Item name="company_id" noStyle>
                                  <Select
                                    showSearch
                                    optionFilterProp="children"
                                    placeholder="Select Company"
                                  >
                                    <Select.Option>ABC</Select.Option>
                                    <Select.Option>EFG</Select.Option>
                                  </Select>
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-2">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Batch No.
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="batch_number"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input title!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input/>
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-3">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Expiry Date
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="expiry_date"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input Expiry Date!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <DatePicker
                                    onChange={onDateChange}
                                    style={{ width: 200 }}
                                  />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                        </div>
                      </div>
                    </TabPane>
                    <TabPane tab="Images & Description" key="2">
                    <div className="tab-content">
                        <h5 className="tab-title">Images & Description</h5>
                        <div className="tab-inner-content">
                        <div className="row">
                        <div className="col-md-5">
                          <Form.Item label="Image Upload">
                            <Form.Item name="file" noStyle>
                              <Upload {...photoProps}>
                                <Button>
                                  <UploadOutlined /> Select Images(s)
                                </Button>
                              </Upload>
                            </Form.Item>
                          </Form.Item>
                        </div>
                        <div className="col-md-6 ml-3 my-auto convention">
                          <i>* Multiple images can be uploaded. Size between 500 x 500 and 1500 x 1500 are recommended.</i>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-10">
                          <Form.Item label=" Product Description">
                            <Form.Item name="description" noStyle>
                              <CKEditor
                                editor={ClassicEditor}
                                // data=""
                                onInit={(editor) => {}}
                                onChange={(event, editor) => {
                                  const data = editor.getData();
                                  // console.log({ event, editor, data });
                                  console.log({ data });
                                }}
                                onBlur={(event, editor) => {
                                  // console.log("Blur.", editor);
                                }}
                                onFocus={(event, editor) => {
                                  // console.log("Focus.", editor);
                                }}
                              />
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                        </div>
                    </div>
                    </TabPane>
                    <TabPane tab="Price & Stock" key="3">
                    <div className="tab-content">
                        <h5 className="tab-title">Price & Stock</h5>
                        <div className="tab-inner-content">
                        <div className="row">
                        <div className="col-md-5">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }}>*</span>
                                &nbsp;Cost Price
                              </div>
                            }
                          >
                            <Form.Item
                              name="price"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input price!",
                                },
                              ]}
                              noStyle
                            >
                              <Input />
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-5">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }}>*</span>
                                &nbsp;Selling Price
                              </div>
                            }
                          >
                            <Form.Item
                              name="selling_price"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input price!",
                                },
                              ]}
                              noStyle
                            >
                              <Input />
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-5">
                          <Form.Item label="Product Unit">
                            <Form.Item name="product_unit_id" noStyle>
                              <Select
                                showSearch
                                optionFilterProp="children"
                                placeholder="Select Product Unit"
                              >
                                {productUnitSelect}
                              </Select>
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                        </div>
                    </div>
                    </TabPane>
                  </Tabs>
                </div>
              </StickyContainer> */}
              <div id="myBtn">
                <Button key="1" className="draft-btn">
                  Save Draft
                </Button>
                <Button
                  className="save-btn ml-2"
                  key="3"
                  type="primary"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </div>
            </Form>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

const mapStateToProps = (state) => ({
  categories: state.category.categories,
  companies: state.company.companies,
  brands: state.brand.brands,
  generic_names: state.generic_name.generic_names,
  product_unit: state.product_unit.product_units,
});

export default connect(mapStateToProps, {
  getCategories,
  getBrands,
  getGenericNames,
  getProductUnits,
  addProduct,
  getCompanies,
})(AddProduct);
