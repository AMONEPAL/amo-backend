import React, { useState } from "react";
import { Modal, Form, Input, InputNumber } from "antd";

const ProductVariationForm = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();

  return (
    <Modal
      visible={visible}
      title="Update Product Variation"
      okText="Add"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            onCreate(values);
            form.resetFields();
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please input title",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <div className="row">
          <div className="col">
        <Form.Item
          name="price"
          label="Price"
          rules={[
            {
              required: true,
              message: "Please input price",
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        </div>
        <div className="col">
        <Form.Item
          name="sale_price"
          label="Sale Price"
          rules={[
            {
              required: true,
              message: "Please input sale price",
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        </div>
        <div className="col">
        <Form.Item
          name="inventory"
          label="Inventory"
          rules={[
            {
              required: true,
              message: "Please input inventory",
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        </div>
        </div>
      </Form>
    </Modal>
  );
};

export default ProductVariationForm;
