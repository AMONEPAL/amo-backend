import React, { Fragment, useEffect, useState } from "react";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  PageHeader,
  Menu,
  Spin,
  Popconfirm,
} from "antd";
import { Redirect } from "react-router-dom";
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getProducts } from "../../../actions/product";

const { Content } = Layout;

const ProductStock = (props) => {
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    props.getProducts();
  }, []);

  useEffect(() => {
    if (props.products != null) {
      setLoading(false);
      let data = [];
      props.products.map((element) => {
        element.variation_set.map((variant) => {
          data.push({
            key: variant.id,
            name: variant.title,
            product: element.title,
            company: element.company,
            batch_number: element.batch_number,
            stock: variant.inventory,
          });
        });
      });
      setTableData(data);
    }
  }, [props.products]);

  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: 250,
    },
    {
      title: "Product",
      dataIndex: "product",
      key: "product",
    },
    {
      title: "Company",
      dataIndex: "company",
      key: "company",
    },
    {
      title: "Batch No.",
      dataIndex: "batch_number",
      key: "batch_number",
    },
    {
      title: "Stock",
      dataIndex: "stock",
      key: "stock",
      filters: [
        {
          text: "Below 50",
          value: 50,
        },
        {
          text: "Below 20",
          value: 30,
        },
        {
          text: "Out of Stock",
          value: 0,
        },
      ],
      onFilter: (value, record) => {
        if (record.stock <= value && record.stock > 0) {
          return true;
        } else if (record.stock <= 0) {
          return true;
        }
      },
      filterMultiple: false,
      render: (quantity) => {
        if (quantity <= 50 && quantity > 0) {
          if (quantity <= 20) {
            return <Tag color="orange">{quantity} left</Tag>;
          } else {
            return <Tag color="gold">{quantity} left</Tag>;
          }
        } else if (quantity <= 0) {
          return <Tag color="red">Out of Stock</Tag>;
        } else {
          return <Tag color="green">{quantity} left</Tag>;
        }
      },
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Fragment>
          <span>
            <a
            //   onClick={() => {
            //     handleProductView(record.key);
            //   }}
            >
              <i className="fa fa-eye ml-3"></i>
            </a>
          </span>
          <span style={{ paddingLeft: "10px" }}>
            <a className="delete-popover">
              <Popconfirm
                title="Are you sure want to delete this product?"
                // onConfirm={() => handleDeleteProduct(record.key)}
                okText="Yes"
                cancelText="No"
              >
                <i className="fa fa-trash delete-icon"> </i>
              </Popconfirm>
            </a>
          </span>
        </Fragment>
      ),
    },
  ];

  //   const handleProductView = (id) => {
  //     setProductID(id);
  //     setProductViewClicked(true);
  //   };

  //   const handleDeleteProduct = (e) => {
  //     props.deleteProduct(e);
  //   };

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
              <Breadcrumb.Item>Product Stock</Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Product Stock Details"
              className="site-page-header"
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="table-section spacing">
              <div className="table-responsive">
                {loading ? (
                  <Spin>
                    <Table
                      bordered={true}
                      columns={columns}
                      dataSource={tableData}
                    />
                  </Spin>
                ) : (
                  <Table
                    bordered={true}
                    columns={columns}
                    dataSource={tableData}
                    pagination={{ pageSize: 10 }}
                  />
                )}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

const mapStateToProps = (state) => ({
  products: state.product.products,
});

export default connect(mapStateToProps, { getProducts })(ProductStock);
