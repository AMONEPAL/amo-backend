import React, {useEffect, useState } from 'react'
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { connect } from 'react-redux'
import {
    Layout,
    Breadcrumb,
    Table,
    PageHeader,
    Menu,
    Spin,
    Popconfirm,
    Button,
    Dropdown,
    Form,
    Input,
    Select,
    InputNumber,
  } from "antd";
import { Link } from "react-router-dom";
import UpdateCoupon from './UpdateCoupon';

const { Content } = Layout;
const { Option } = Select;

const Coupons = (props) => {
  const [form] = Form.useForm();
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [percentage, setPercentageSelected] = useState(false);
  const [flat, setFlatSelected] = useState(false);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
      setLoading(false);
      let data = [
        {
          key: 1,
          title: "Dashain Offer",
          type: "Flat Discount",
          code: "DASHAIN2077",
          amount: "Rs. 500",

        },
        {
          key: 2,
          title: "Dashain Offer",
          type: "% Discount",
          code: "DASHAIN2077",
          amount: "35%",

        }
      ];
      setTableData(data);
  }, []);

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Type",
      dataIndex: "type",
      key: "type",
    },
    {
      title: "Code",
      dataIndex: "code",
      key: "code",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
      },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <>
        <span>
              <a
              onClick={() => {
                openUpdateModal(record.key);
              }}
            >
              <i className="fa fa-edit edit-icon ml-3"></i>
            </a>
          </span>
          <span style={{ paddingLeft: "10px" }}>
              <a className="delete-popover">
                <Popconfirm
                  title="Are you sure want to delete this coupen?"
                  okText="Yes"
                  cancelText="No"
                >
                  <i className="fa fa-trash delete-icon"> </i>
                </Popconfirm>
              </a>
            </span>
        </>
      ),
    },
  ];
  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );
  const handleTypeSelect = (val) => {
    if (val == "flat") {
      setFlatSelected(true);
      setPercentageSelected(false);
    }

    if (val == "percentage") {
      setPercentageSelected(true);
      setFlatSelected(false);
    } 
  }

  const onFinish = (values) => {
    console.log(values);
  }

  const openUpdateModal = (val) => {
    setVisible(true);
  };

  const onUpdate = (finalData) => {
    setVisible(false);
  };

  return (
    <>
    <Layout>
        <SideNav className="side-nav-fixed" />
        <Layout className="site-layout">
        <TopHeader />
        <Content
            className="site-layout-background"
        >
            <Breadcrumb className="admin-breadcrumb">
            <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
            <Breadcrumb.Item>
                <Link to="/coupons">Coupons</Link>
            </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
            title="Manage Coupons"
            className="site-page-header"
            extra={[
                <Button key="2" className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
          <div className="row">
      <div className="col-sm-4">
        <Form
          layout="vertical"
          form={form}
          name="brandForm"
          onFinish={onFinish}
        >
            <div className="tab-section spacing">
              <div className="tab-content">
                <h5 className="sidebar-title">Create Coupon</h5>
                <div className="sidebar-inner-content">
                  <div className="row">
                    <div className="col-md-11">
                      <Form.Item
                        label={
                          <div>
                            <span
                              style={{ color: "red" }}
                              className="mr-1"
                            >
                              *
                            </span>
                            Coupon Title
                          </div>
                        }
                      >
                        <Form.Item
                          name="title"
                          rules={[
                            {
                              required: true,
                              message: "Please input title!",
                            },
                          ]}
                          noStyle
                        >
                          <Input />
                        </Form.Item>
                      </Form.Item>
                    </div>
                    <div className="col-md-11">
                    <Form.Item
                        label={
                          <div>
                            <span
                              style={{ color: "red" }}
                              className="mr-1"
                            >
                              *
                            </span>
                            Coupon Code
                          </div>
                        }
                      >
                        <Form.Item
                          name="code"
                          rules={[
                            {
                              required: true,
                              message: "Please input code!",
                            },
                          ]}
                          noStyle
                        >
                          <Input />
                        </Form.Item>
                      </Form.Item>
                    </div>
                    <div className="col-md-11">
                    <Form.Item
                        label={
                          <div>
                            <span
                              style={{ color: "red" }}
                              className="mr-1"
                            >
                              *
                            </span>
                            Coupon Type
                          </div>
                        }
                      >
                        <Form.Item
                          name="type"
                          rules={[
                            {
                              required: true,
                              message: "Please select coupen type",
                            },
                          ]}
                          noStyle
                        >
                          <Select
                            showSearch
                            placeholder="Select Type"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                            onChange = {handleTypeSelect}
                          >
                            <Option value="flat">Flat Discount</Option>
                            <Option value="percentage">% Discount</Option>
                          </Select>
                        </Form.Item>
                      </Form.Item>
                    </div>
                    { flat ? 
                    <div className="col-md-11">
                    <Form.Item
                      label={
                        <div>
                          <span
                            style={{ color: "red" }}
                            className="mr-1"
                          >
                            *
                          </span>
                          Flat Amount
                        </div>
                      }
                    >
                      <Form.Item
                        name="amount"
                        rules={[
                          {
                            required: true,
                            message: "Please provide amount.",
                          },
                        ]}
                        noStyle
                      >
                        <InputNumber
                          min={0}
                          max={100000}
                          formatter={(value) => `Rs.${value}`}
                          parser={(value) => value.replace("Rs.", "")}
                          className="input-number"
                        />
                      </Form.Item>
                    </Form.Item>
                  </div> 
                  :
                  ""
                  }
                  { percentage ? 
                    <div className="col-md-11">
                    <Form.Item
                      label={
                        <div>
                          <span
                            style={{ color: "red" }}
                            className="mr-1"
                          >
                            *
                          </span>
                          Actual Percentage
                        </div>
                      }
                    >
                      <Form.Item
                        name="amount"
                        rules={[
                          {
                            required: true,
                            message: "Please provide %.",
                          },
                        ]}
                        noStyle
                      >
                       <InputNumber
                          min={1}
                          max={100}
                          formatter={(value) => `${value}%`}
                          parser={(value) => value.replace("%", "")}
                          className="input-number"
                        />
                      </Form.Item>
                    </Form.Item>
                  </div> 
                  :
                  ""
                  }
                  </div>
                  <div className="row">
                    <div className="col-md-11">
                      <Form.Item>
                        <Button type="primary" htmlType="submit" className="save-btn">
                          SAVE
                        </Button>
                      </Form.Item>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </Form>
      </div>
      <div className="col">
        {loading ? (
          <div className="table-section spacing">
            <div className="table-responsive">
              <Spin size="large">
                <Table
                  bordered={true}
                  columns={columns}
                  dataSource={tableData}
                />
              </Spin>
            </div>
          </div>
        ) : (
          <div className="table-section spacing">
            <div className="table-responsive">
              <Table
                bordered={true}
                columns={columns}
                dataSource={tableData}
              />
            </div>
          </div>
        )}
      </div>
    </div>
    <UpdateCoupon
        visible={visible}
        onCreate={onUpdate}
        onCancel={() => {
          setVisible(false);
        }}
      />
        </Content>
        </Layout>
    </Layout>
</>
)
}

const mapStateToProps = (state) => ({
    
})

export default connect(mapStateToProps, null)(Coupons)