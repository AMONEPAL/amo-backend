import React, {useEffect, useState } from 'react'
import SideNav from "../../dashboard/SideNav";
import TopHeader from "../../dashboard/TopHeader";
import { connect } from 'react-redux'
import {
    Layout,
    Breadcrumb,
    Table,
    Tag,
    PageHeader,
    Menu,
    Spin,
    Popconfirm,
    Button,
    Dropdown,
  } from "antd";
import { Link } from "react-router-dom";

const { Content } = Layout;

const Coupons = (props) => {
    const exportoption = (
        <Menu>
          <Menu.Item>
            <a href="#">PRINT</a>
          </Menu.Item>
          <Menu.Item>
            <a href="#">EXCEL</a>
          </Menu.Item>
          <Menu.Item>
            <a href="#">PDF</a>
          </Menu.Item>
        </Menu>
      );
        return (
            <>
            <Layout>
                <SideNav className="side-nav-fixed" />
                <Layout className="site-layout">
                <TopHeader />
                <Content
                    className="site-layout-background"
                >
                    <Breadcrumb className="admin-breadcrumb">
                    <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <Link to="/coupons">Coupons</Link>
                    </Breadcrumb.Item>
                    </Breadcrumb>
                    <PageHeader
                    title="Manage Coupons"
                    className="site-page-header"
                    extra={[
                        <Button key="2" className="extra-btn">
                          <Dropdown overlay={exportoption}>
                            <a
                              className="ant-dropdown-link"
                              onClick={(e) => e.preventDefault()}
                            >
                              Export <i className="fas fa-caret-down"></i>
                            </a>
                          </Dropdown>
                        </Button>,
                      ]}
                    ></PageHeader>
                  
                </Content>
                </Layout>
            </Layout>
        </>
        )
}

const mapStateToProps = (state) => ({
    
})

export default connect(mapStateToProps, null)(Coupons)