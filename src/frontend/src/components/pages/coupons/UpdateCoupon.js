import React, { useState } from "react";
import { Modal, Form, Input, InputNumber, Select } from "antd";
import { connect } from "react-redux";

const { Option } = Select;

const UpdateCoupon = ({ visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();
  const [percentage, setPercentageSelected] = useState(false);
  const [flat, setFlatSelected] = useState(false);

  const handleTypeSelect = (val) => {
    if (val == "flat") {
      setFlatSelected(true);
      setPercentageSelected(false);
    }

    if (val == "percentage") {
      setPercentageSelected(true);
      setFlatSelected(false);
    } 
  }
  return (
    <Modal
      visible={visible}
      title="Update Coupon"
      okText="Update"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            // console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
                      <Form.Item
                        label={
                          <div>
                            <span
                              style={{ color: "red" }}
                              className="mr-1"
                            >
                              *
                            </span>
                            Coupon Title
                          </div>
                        }
                      >
                        <Form.Item
                          name="title"
                          rules={[
                            {
                              required: true,
                              message: "Please input title!",
                            },
                          ]}
                          noStyle
                        >
                          <Input />
                        </Form.Item>
                      </Form.Item>
                    <Form.Item
                        label={
                          <div>
                            <span
                              style={{ color: "red" }}
                              className="mr-1"
                            >
                              *
                            </span>
                            Coupon Code
                          </div>
                        }
                      >
                        <Form.Item
                          name="code"
                          rules={[
                            {
                              required: true,
                              message: "Please input code!",
                            },
                          ]}
                          noStyle
                        >
                          <Input />
                        </Form.Item>
                      </Form.Item>
                    <Form.Item
                        label={
                          <div>
                            <span
                              style={{ color: "red" }}
                              className="mr-1"
                            >
                              *
                            </span>
                            Coupon Type
                          </div>
                        }
                      >
                        <Form.Item
                          name="type"
                          rules={[
                            {
                              required: true,
                              message: "Please select coupen type",
                            },
                          ]}
                          noStyle
                        >
                          <Select
                            showSearch
                            placeholder="Select Type"
                            optionFilterProp="children"
                            filterOption={(input, option) =>
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                            }
                            onChange = {handleTypeSelect}
                          >
                            <Option value="flat">Flat Discount</Option>
                            <Option value="percentage">% Discount</Option>
                          </Select>
                        </Form.Item>
                      </Form.Item>
                    { flat ? 
                    <Form.Item
                      label={
                        <div>
                          <span
                            style={{ color: "red" }}
                            className="mr-1"
                          >
                            *
                          </span>
                          Flat Amount
                        </div>
                      }
                    >
                      <Form.Item
                        name="amount"
                        rules={[
                          {
                            required: true,
                            message: "Please provide amount.",
                          },
                        ]}
                        noStyle
                      >
                        <InputNumber
                          min={0}
                          max={100000}
                          formatter={(value) => `Rs.${value}`}
                          parser={(value) => value.replace("Rs.", "")}
                          className="input-number"
                        />
                      </Form.Item>
                    </Form.Item>
                  :
                  ""
                  }
                  { percentage ? 
                    <Form.Item
                      label={
                        <div>
                          <span
                            style={{ color: "red" }}
                            className="mr-1"
                          >
                            *
                          </span>
                          Actual Percentage
                        </div>
                      }
                    >
                      <Form.Item
                        name="amount"
                        rules={[
                          {
                            required: true,
                            message: "Please provide %.",
                          },
                        ]}
                        noStyle
                      >
                       <InputNumber
                          min={1}
                          max={100}
                          formatter={(value) => `${value}%`}
                          parser={(value) => value.replace("%", "")}
                          className="input-number"
                        />
                      </Form.Item>
                    </Form.Item>
                  :
                  ""
                  }
      </Form>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  single_brand: state.brand.brand,
});

export default connect(mapStateToProps)(UpdateCoupon);
