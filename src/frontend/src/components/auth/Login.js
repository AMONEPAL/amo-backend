import React from "react";
import { Redirect } from "react-router-dom";
import { Form, Input, Button, Checkbox } from "antd";
import { connect } from "react-redux";
import { login } from "../../actions/auth";

const Login = (props) => {
  const [form] = Form.useForm();
  const onFinish = (val) => {
    props.login(val.phone, val.password);
  };

  if (props.isAuthenticated) {
    return <Redirect to="/" />;
  }

  return (
    <>
    <div className="container-fluid">
        <div className="row">
          <div className="col-sm-6 login-section-wrapper">
            <div className="login-wrapper my-auto">
              <h5 className="login-title">Log In as Admin.</h5>
                <Form
                    onFinish={onFinish}
                    className="login-form"
                    form={form}
                    layout="vertical"
                  >
                  <div className="form-group">
                    <Form.Item
                    label="Phone Number"
                      name="phone"
                      rules={[
                        {
                          required: false,
                          message: "Please input your phone number!",
                        },
                      ]}
                    >
                      <Input
                        className="form-control"
                        placeholder="Enter your Phone Number"
                      />
                    </Form.Item>
                    </div>
                    <Form.Item
                      label="Password"
                      name="password"
                      rules={[{ required: false, message: "Please input your password!" }]}
                    >
                      <Input
                        type="password"
                        className="form-control"
                        placeholder="Enter your Passsword"
                      />
                    </Form.Item>
                    {/* <div>
                        <Checkbox className="remember-me">Remember me</Checkbox>
                      </div> */}
                    <Form.Item>
                      <Button
                      type="default"
                      htmlType="submit"
                      className="login-form-button"
                    >
                      Log in
                    </Button>
                    </Form.Item>
                  </Form>
            </div>
          </div>
          <div className="col-sm-6 px-0 d-none d-sm-block">
            <img src="../../static/images/auth.jpg" alt="login image" className="login-img" />
          </div>
        </div>
    </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticated: state.auth.isAuthenticated,
  auth: state.auth,
});

export default connect(mapStateToProps, { login })(Login);
