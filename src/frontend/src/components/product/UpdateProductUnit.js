import React, { useEffect } from "react";
import { Modal, Form, Input } from "antd";
import { connect } from "react-redux";

const UpdateProductUnit = ({ single_unit, visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (single_unit != null) {
      single_unit = single_unit.title;
      form.setFieldsValue({
        title: single_unit,
      });
    }
  }, [single_unit]);
  return (
    <Modal
      visible={visible}
      title="Update Product Unit"
      okText="Update"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please input title",
            },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  single_unit: state.product_unit.product_unit,
});

export default connect(mapStateToProps)(UpdateProductUnit);
