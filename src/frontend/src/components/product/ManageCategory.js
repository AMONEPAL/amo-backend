import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  Button,
  PageHeader,
  Dropdown,
  Menu,
  Spin,
  Form,
  Input,
  Select,
  message,
  Upload,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { StickyContainer, Sticky } from "react-sticky";
import SideNav from "../dashboard/SideNav";
import TopHeader from "../dashboard/TopHeader";
import { Link } from "react-router-dom";

import { getCategories } from "../../actions/category";

const { Content } = Layout;

const ManageCategory = (props) => {
  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const [tableData, setTableData] = useState([]);
  const [categorySelect, setCategorySelect] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    props.getCategories();
  }, []);

  useEffect(() => {
    if (props.categories != null) {
      setLoading(false);
      let data = [];
      props.categories.map((element) => {
        data.push({
          key: element.id,
          title: element.title,
          status: ["active"],
        });
      });
      setTableData(data);

      setCategorySelect(
        props.categories.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
    }
  }, [props.categories]);

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "name",
      width: 150,
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      width: 150,
      render: (tags) => (
        <>
          {tags.map((tag) => {
            let color;
            if (tag === "active") {
              color = "green";
            }
            if (tag === "draft") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      width: 150,
      render: (text, record) => <>-</>,
    },
  ];

  const photoProps = {
    action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    listType: "picture",

    onChange(info) {
      if (info.file.status !== "uploading") {
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    progress: {
      strokeColor: {
        "0%": "#108ee9",
        "100%": "#87d068",
      },
      strokeWidth: 3,
      format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
    },
  };

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Products</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-category">Manage Category</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Product Categories"
              className="site-page-header"
              extra={[
                <Button key="2"  className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="row">
              <div className="col-sm-4">
                <Form
                  layout="vertical"
                  name="categoryForm"
                  //   onFinish={onFinish}
                  //   onFinishFailed={onFinishFailed}
                >
                  <StickyContainer>
                    <div className="tab-section spacing">
                      <div className="tab-content">
                        <h5 className="sidebar-title">Add New Category</h5>
                        <div className="sidebar-inner-content">
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Title
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input title!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Slug
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="slug"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input slug!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item label="Parent Category">
                                <Form.Item name="parent_category" noStyle>
                                  <Select
                                    showSearch
                                    optionFilterProp="children"
                                    placeholder="Select Parent Category"
                                  >
                                    {categorySelect}
                                  </Select>
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item label="Description">
                                <Form.Item name="description" noStyle>
                                  <Input.TextArea />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item label="Image">
                                <Form.Item name="file" noStyle>
                                  <Upload {...photoProps}>
                                    <Button>
                                      <UploadOutlined /> Select Files
                                    </Button>
                                  </Upload>
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item>
                                <Button type="primary" htmlType="submit" className="save-btn">
                                SAVE
                                </Button>
                              </Form.Item>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </StickyContainer>
                </Form>
              </div>
              <div className="col">
                {loading ? (
                  <div className="table-section spacing">
                    <div className="table-responsive">
                      <Spin size="large">
                        <Table
                          bordered={true}
                          columns={columns}
                          dataSource={tableData}
                        />
                      </Spin>
                    </div>
                  </div>
                ) : (
                  <div className="table-section spacing">
                    <div className="table-responsive">
                      <Table
                        bordered={true}
                        columns={columns}
                        dataSource={tableData}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

const mapStateToProps = (state) => ({
  categories: state.category.categories,
});

export default connect(mapStateToProps, { getCategories })(ManageCategory);
