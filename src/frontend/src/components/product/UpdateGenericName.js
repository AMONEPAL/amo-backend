import React, { useEffect } from "react";
import { Modal, Form, Input } from "antd";
import { connect } from "react-redux";

const updateGenericName = ({ single_generic_name, visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (single_generic_name != null) {
      single_generic_name = single_generic_name.title;
      form.setFieldsValue({
        title: single_generic_name,
      });
    }
  }, [single_generic_name]);
  return (
    <Modal
      visible={visible}
      title="Update Generic Name"
      okText="Update"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please input title",
            },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  single_generic_name: state.generic_name.generic_name,
});

export default connect(mapStateToProps)(updateGenericName);
