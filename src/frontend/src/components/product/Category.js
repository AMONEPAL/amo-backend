import React, { useEffect, useState } from "react";
import {
  Form,
  Input,
  Select,
  Checkbox,
  Button,
  Card,
  Upload,
  message,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import { connect } from "react-redux";
import { getCategories, addCategory } from "../../actions/category";

const Category = (props) => {
  useEffect(() => {
    props.getCategories();
  }, []);

  useEffect(() => {
    props.categories != null &&
      setCategorySelect(
        props.categories.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
  }, [props.categories]);

  const [form] = Form.useForm();
  const [categorySelect, setCategorySelect] = useState(null);

  const photoProps = {
    action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    listType: "picture",

    onChange(info) {
      if (info.file.status !== "uploading") {
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    progress: {
      strokeColor: {
        "0%": "#108ee9",
        "100%": "#87d068",
      },
      strokeWidth: 3,
      format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
    },
  };

  const onFinish = (val) => {
    let form_data = new FormData();
    let files = [];

    try {
      files = val.file.fileList;
    } catch (error) {}

    files.length > 0
      ? files.map((element, i) => {
          if (element.status === "done") {
            form_data.append("file", element.originFileObj);
          }
        })
      : form_data.append("file", "");

    let data = {
      title: val.title,
      slug: val.slug,
      product_set: [],
      description: val.description || "",
    };

    Object.keys(data).map((key) => form_data.append(key, data[key]));

    props.addCategory(form_data);
    form.resetFields();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div style={{ width: "500px" }}>
      <Card>
        <h2>Add Category</h2>
        <Form
          form={form}
          name="categoryForm"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item
            label={
              <div>
                <span style={{ color: "red" }}>*</span>&nbsp;Name
              </div>
            }
            extra="Some desc"
          >
            <Form.Item
              name="title"
              rules={[
                {
                  required: true,
                  message: "Please input title!",
                },
              ]}
              noStyle
            >
              <Input />
            </Form.Item>
          </Form.Item>

          <Form.Item
            label={
              <div>
                <span style={{ color: "red" }}>*</span>&nbsp;Slug
              </div>
            }
          >
            <Form.Item
              name="slug"
              rules={[
                {
                  required: true,
                  message: "Please input slug!",
                },
              ]}
              noStyle
            >
              <Input />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Parent Category">
            <Form.Item name="parent_category" noStyle>
              <Select showSearch optionFilterProp="children">
                {categorySelect}
              </Select>
            </Form.Item>
          </Form.Item>

          <Form.Item label="Description">
            <Form.Item name="description" noStyle>
              <Input.TextArea />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Active">
            <Form.Item name="active" valuePropName="checked" noStyle>
              <Checkbox></Checkbox>
            </Form.Item>
          </Form.Item>

          <Form.Item label="File">
            <Form.Item name="file" noStyle>
              <Upload {...photoProps}>
                <Button>
                  <UploadOutlined /> Select Files
                </Button>
              </Upload>
            </Form.Item>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Add
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

const mapStateToProps = (state) => ({
  categories: state.category.categories,
});

export default connect(mapStateToProps, { getCategories, addCategory })(
  Category
);
