import React, { useEffect, useState } from "react";
import { Form, Input, Select, Button, Card, Upload, message } from "antd";
import { connect } from "react-redux";
import { addProduct } from "../../actions/product";
import { UploadOutlined } from "@ant-design/icons";
import { getCategories } from "../../actions/category";


const ProductForm = (props) => {
  useEffect(() => {
    props.getCategories();
  }, []);

  useEffect(() => {
    props.categories != null &&
      setCategorySelect(
        props.categories.map((element, i) => {
          return (
            <Select.Option key={i} value={element.id}>
              {element.title}
            </Select.Option>
          );
        })
      );
  }, [props.categories]);

  const [form] = Form.useForm();
  const [categorySelect, setCategorySelect] = useState(null);

  const photoProps = {
    action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
    listType: "picture",

    onChange(info) {
      if (info.file.status !== "uploading") {
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    progress: {
      strokeColor: {
        "0%": "#108ee9",
        "100%": "#87d068",
      },
      strokeWidth: 3,
      format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
    },
  };

  const onFinish = (val) => {
    let form_data = new FormData();
    let files = [];

    try {
      files = val.file.fileList;
    } catch (error) {}

    files.length > 0
      ? files.map((element, i) => {
          if (element.status === "done") {
            form_data.append("file", element.originFileObj);
          }
        })
      : form_data.append("file", "");

    let data = {
      product_title: val.product_title,
      price: val.price,
      description: val.description || "",
      category_id: val.category_id || "",
      brand_id: val.brand_id || "",
      product_unit_id: val.product_unit_id || "",
      generic_names_id: val.generic_names_id || "",
      company_id: val.company_id || "",
    };

    Object.keys(data).map((key) => form_data.append(key, data[key]));

    props.addProduct(form_data);
    form.resetFields();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div style={{ width: "500px" }}>
      <Card>
        <h2>Add Product</h2>
        <Form
          form={form}
          name="productForm"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
        >
          <Form.Item
            label={
              <div>
                <span style={{ color: "red" }}>*</span>&nbsp;Name
              </div>
            }
            extra="Some desc"
          >
            <Form.Item
              name="product_title"
              rules={[
                {
                  required: true,
                  message: "Please input title!",
                },
              ]}
              noStyle
            >
              <Input />
            </Form.Item>
          </Form.Item>

          <Form.Item
            label={
              <div>
                <span style={{ color: "red" }}>*</span>&nbsp;Price
              </div>
            }
          >
            <Form.Item
              name="price"
              rules={[
                {
                  required: true,
                  message: "Please input price!",
                },
              ]}
              noStyle
            >
              <Input />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Unit">
            <Form.Item name="product_unit_id" noStyle>
              <Select showSearch optionFilterProp="children"></Select>
            </Form.Item>
          </Form.Item>

          <Form.Item label="Description">
            <Form.Item name="description" noStyle>
              <Input.TextArea />
            </Form.Item>
          </Form.Item>

          <Form.Item label="Category">
            <Form.Item name="category_id" noStyle>
              <Select
                showSearch
                optionFilterProp="children"
                placeholder="Select Category"
              >
                {categorySelect}
              </Select>
            </Form.Item>
          </Form.Item>

          <Form.Item label="Brand">
            <Form.Item name="brand_id" noStyle>
              <Select showSearch optionFilterProp="children"></Select>
            </Form.Item>
          </Form.Item>

          <Form.Item label="Generic Name">
            <Form.Item name="generic_names_id" noStyle>
              <Select showSearch optionFilterProp="children"></Select>
            </Form.Item>
          </Form.Item>

          <Form.Item label="Company">
            <Form.Item name="company_id" noStyle>
              <Select showSearch optionFilterProp="children"></Select>
            </Form.Item>
          </Form.Item>

          <Form.Item label="File">
            <Form.Item name="file" noStyle>
              <Upload {...photoProps}>
                <Button>
                  <UploadOutlined /> Select Files
                </Button>
              </Upload>
            </Form.Item>
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Add
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

const mapStateToProps = (state) => ({
  categories: state.category.categories,
});

export default connect(mapStateToProps, { addProduct, getCategories })(
  ProductForm
);
