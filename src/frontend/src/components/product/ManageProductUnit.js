import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  Button,
  PageHeader,
  Dropdown,
  Menu,
  Spin,
  Form,
  Input,
  Popconfirm,
} from "antd";
import SideNav from "../dashboard/SideNav";
import TopHeader from "../dashboard/TopHeader";
import { Link } from "react-router-dom";

import { getProductUnits, addProductUnit, getProductUnit, updateProductUnit, deleteProductUnit } from "../../actions/product_unit";
import UpdateProductUnit from "./UpdateProductUnit";

const { Content } = Layout;

const ManageProductUnit = (props) => {
  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const [form] = Form.useForm();
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);
  const [unitId, setUnitId] = useState();

  useEffect(() => {
    props.getProductUnits();
  }, []);

  useEffect(() => {
    if (props.product_units != null) {
      setLoading(false);
      let data = [];
      props.product_units.map((element) => {
        data.push({
          key: element.id,
          title: element.title,
          status: ["active"],
        });
      });
      setTableData(data);
    }
  }, [props.product_units]);

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "name",
      width: 150,
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      width: 150,
      render: (tags) => (
        <>
          {tags.map((tag) => {
            let color;
            if (tag === "active") {
              color = "green";
            }
            if (tag === "draft") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      width: 150,
      render: (text, record) => (
        <>
          <span>
            <a
              onClick={() => {
                openUpdateModal(record.key);
              }}
            >
              <i className="fa fa-edit edit-icon"></i>
            </a>
          </span>

          <span style={{ paddingLeft: "10px" }}>
            <a className="delete-popover">
              <Popconfirm
                title="Are you sure want to delete this product unit?"
                onConfirm={() => handleDelete(record.key)}
                okText="Yes"
                cancelText="No"
              >
                <i className="fa fa-trash delete-icon"> </i>
              </Popconfirm>
            </a>
          </span>
        </>
      ),
    },
  ];

  const onFinish = (val) => {
    let data = {
      title: val.title,
    };

    props.addProductUnit(data);
    form.resetFields();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const openUpdateModal = (val) => {
    setVisible(true);
    setUnitId(val);
    props.getProductUnit(val);
  };

  const onUpdate = (finalData) => {
    console.log("Received values of form: ", finalData);
    props.updateProductUnit(unitId, finalData);
    setVisible(false);
  };

  const handleDelete = (e) => {
    props.deleteProductUnit(e);
  }

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Products</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-product-unit">Manage Product Unit</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Product Units"
              className="site-page-header"
              extra={[
                <Button key="2" className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="row">
              <div className="col-md-4">
                <Form
                  layout="vertical"
                  form={form}
                  name="productUnitForm"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                >
                    <div className="tab-section spacing">
                      <div className="tab-content">
                        <h5 className="sidebar-title">Add New Product Unit</h5>
                        <div className="sidebar-inner-content">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Title
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input title!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                              <Form.Item>
                                <Button type="primary" htmlType="submit" className="save-btn">
                                  SAVE
                                </Button>
                              </Form.Item>
                        </div>
                      </div>
                    </div>
                </Form>
              </div>
              <div className="col-md-8">
                {loading ? (
                  <div className="table-section spacing">
                    <div className="table-responsive">
                      <Spin size="large">
                        <Table
                          bordered={true}
                          columns={columns}
                          dataSource={tableData}
                        />
                      </Spin>
                    </div>
                  </div>
                ) : (
                  <div className="table-section spacing">
                    <div className="table-responsive">
                      <Table
                        bordered={true}
                        columns={columns}
                        dataSource={tableData}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
      <UpdateProductUnit
        visible={visible}
        onCreate={onUpdate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </>
  );
};

const mapStateToProps = (state) => ({
  product_units: state.product_unit.product_units,
});

export default connect(mapStateToProps, {
  getProductUnits,
  getProductUnit,
  updateProductUnit,
  addProductUnit,
  deleteProductUnit,
})(ManageProductUnit);
