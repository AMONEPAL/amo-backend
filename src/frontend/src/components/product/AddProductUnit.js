import React, { useState } from "react";
import { connect } from "react-redux";
import { addProductUnit } from "../../actions/product_unit";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  Button,
  Space,
  Select,
  PageHeader,
  Dropdown,
  Menu,
  Tabs,
  Form,
  Input,
  Upload,
  Checkbox,
} from "antd";
import { StickyContainer, Sticky } from "react-sticky";
import SideNav from "../dashboard/SideNav";
import TopHeader from "../dashboard/TopHeader";
import { Link } from "react-router-dom";

import { UploadOutlined } from "@ant-design/icons";

const { Content } = Layout;
const { TabPane } = Tabs;
const { Option } = Select;

const renderTabBar = (props, DefaultTabBar) => (
  <Sticky bottomOffset={80}>
    {({ style }) => (
      <DefaultTabBar
        {...props}
        className="site-custom-tab-bar"
        style={{ ...style }}
      />
    )}
  </Sticky>
);

const AddProductUnit = (props) => {
  const [form] = Form.useForm();

  const onFinish = (val) => {
    let data = {
      title: val.title,
    };

    props.addProductUnit(data);
    form.resetFields();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Products</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-product-unit">Manage Product Units</Link>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-product-unit/add">Add Product Unit</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Add Product Unit"
              className="site-page-header"
              extra={[<Button key="1">Import</Button>]}
            ></PageHeader>
            <Form
              layout="vertical"
              form={form}
              name="productUnitForm"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              <StickyContainer>
                <div className="tab-section spacing">
                  <div className="tab-content">
                    <h5 className="tab-title">Basic Information</h5>
                    <div className="tab-inner-content">
                      <div className="row">
                        <div className="col-md-3">
                          <Form.Item
                            label={
                              <div>
                                <span style={{ color: "red" }} className="mr-1">
                                  *
                                </span>
                                Title
                              </div>
                            }
                          >
                            <Form.Item
                              name="title"
                              rules={[
                                {
                                  required: true,
                                  message: "Please input title!",
                                },
                              ]}
                              noStyle
                            >
                              <Input />
                            </Form.Item>
                          </Form.Item>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </StickyContainer>
              <div id="myBtn">
                <Button key="1">Save Draft</Button>
                <Button
                  key="3"
                  type="primary"
                  htmlType="submit"
                  className="ml-2"
                >
                  {/* <a href="#" className="btn-link"> */}
                  Submit
                  {/* </a> */}
                </Button>
              </div>
            </Form>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default connect(null, { addProductUnit })(AddProductUnit);
