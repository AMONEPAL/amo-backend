import React, { Component, Fragment } from "react";
import { withAlert } from "react-alert";
import { connect } from "react-redux";
import PropTypes from "prop-types";

export class Alert extends Component {
  static propTypes = {
    message: PropTypes.object.isRequired,
  };

  componentDidUpdate(prevProps) {
    const { alert, message } = this.props;

    if (message.error_message !== null) {
      alert.error(message.error_message);
    }

    if (message.success_message !== prevProps.message.success_message) {
      if (message.success_message !== null) {
        //auth
        if (message.success_message.loggedIn)
          alert.success(message.success_message.loggedIn);
        if (message.success_message.loggedOut)
          alert.success(message.success_message.loggedOut);

        //ProductUnit
        if (message.success_message.addProductUnit)
          alert.success(message.success_message.addProductUnit);
        if (message.success_message.updateProductUnit)
          alert.success(message.success_message.updateProductUnit);
        if (message.success_message.deleteProductUnit)
          alert.success(message.success_message.deleteProductUnit);

        //Generic Name
        if (message.success_message.addGenericName)
          alert.success(message.success_message.addGenericName);
        if (message.success_message.updateGenericName)
          alert.success(message.success_message.updateGenericName);
        if (message.success_message.deleteGenericName)
          alert.success(message.success_message.deleteGenericName);

        //Brand Name
        if (message.success_message.addBrand)
          alert.success(message.success_message.addBrand);
        if (message.success_message.updateBrand)
          alert.success(message.success_message.updateBrand);
        if (message.success_message.deleteBrand)
          alert.success(message.success_message.deleteBrand);

        //Company
        if (message.success_message.addCompany)
          alert.success(message.success_message.addCompany);
        if (message.success_message.updateCompany)
          alert.success(message.success_message.updateCompany);
        if (message.success_message.deleteCompany)
          alert.success(message.success_message.deleteCompany);

        //Product Variation
        if (message.success_message.addProductVariation)
          alert.success(message.success_message.addProductVariation);
        if (message.success_message.updateProductVariation)
          alert.success(message.success_message.updateProductVariation);
      }
    }
  }
  render() {
    return <Fragment />;
  }
}

const mapStateToProps = (state) => ({
  message: state.alert,
});

export default connect(mapStateToProps)(withAlert()(Alert));
