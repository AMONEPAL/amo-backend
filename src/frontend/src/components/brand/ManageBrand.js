import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Layout,
  Breadcrumb,
  Table,
  Tag,
  Button,
  PageHeader,
  Dropdown,
  Menu,
  Spin,
  Form,
  Input,
  Popconfirm,
} from "antd";
import { StickyContainer, Sticky } from "react-sticky";
import SideNav from "../dashboard/SideNav";
import TopHeader from "../dashboard/TopHeader";
import { Link } from "react-router-dom";
import UpdateBrand from "./UpdateBrand";
import { getBrands, addBrand, updateBrand, getBrand, deleteBrand } from "../../actions/brand";

const { Content } = Layout;

const ManageBrand = (props) => {
  const exportoption = (
    <Menu>
      <Menu.Item>
        <a href="#">PRINT</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">EXCEL</a>
      </Menu.Item>
      <Menu.Item>
        <a href="#">PDF</a>
      </Menu.Item>
    </Menu>
  );

  const [form] = Form.useForm();
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);
  const [brandId, setBrandId] = useState();

  useEffect(() => {
    props.getBrands();
  }, []);

  useEffect(() => {
    if (props.brands != null) {
      setLoading(false);
      let data = [];
      props.brands.map((element) => {
        data.push({
          key: element.id,
          title: element.title,
          status: ["active"],
        });
      });
      setTableData(data);
    }
  }, [props.brands]);

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "name",
      width: 150,
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      width: 150,
      render: (tags) => (
        <>
          {tags.map((tag) => {
            let color;
            if (tag === "active") {
              color = "green";
            }
            if (tag === "draft") {
              color = "volcano";
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Action",
      key: "action",
      width: 150,
      render: (text, record) => (
        <>
        <span>
              <a
                onClick={() => {
                  openUpdateModal(record.key);
                }}
              >
                <i className="fa fa-edit edit-icon"></i>
              </a>
            </span>
  
            <span style={{ paddingLeft: "10px" }}>
              <a className="delete-popover">
                <Popconfirm
                  title="Are you sure want to delete this brand?"
                  onConfirm={() => handleDelete(record.key)}
                  okText="Yes"
                  cancelText="No"
                >
                  <i className="fa fa-trash delete-icon"> </i>
                </Popconfirm>
              </a>
            </span>
        </>
        ),
    },
  ];

  const onFinish = (val) => {
    let data = {
      title: val.title,
      description: val.description || "",
    };

    props.addBrand(data);
    form.resetFields();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const openUpdateModal = (val) => {
    setVisible(true);
    setBrandId(val);
    props.getBrand(val);
  };

  const onUpdate = (finalData) => {
    props.updateBrand(brandId, finalData);
    setVisible(false);
  };

  const handleDelete = (e) => {
    props.deleteBrand(e);
  }

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>Brands</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/manage-brand">Manage Brands</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Manage Brands"
              className="site-page-header"
              extra={[
                <Button key="2" className="extra-btn">
                  <Dropdown overlay={exportoption}>
                    <a
                      className="ant-dropdown-link"
                      onClick={(e) => e.preventDefault()}
                    >
                      Export <i className="fas fa-caret-down"></i>
                    </a>
                  </Dropdown>
                </Button>,
              ]}
            ></PageHeader>
            <div className="instant-section"></div>
            <div className="row">
              <div className="col-sm-4">
                <Form
                  layout="vertical"
                  form={form}
                  name="brandForm"
                  onFinish={onFinish}
                  onFinishFailed={onFinishFailed}
                >
                  <StickyContainer>
                    <div className="tab-section spacing">
                      <div className="tab-content">
                        <h5 className="sidebar-title">Add New Brand</h5>
                        <div className="sidebar-inner-content">
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Title
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="title"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input title!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item label="Description">
                                <Form.Item name="description" noStyle>
                                  <Input.TextArea />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-11">
                              <Form.Item>
                                <Button type="primary" htmlType="submit" className="save-btn">
                                  SAVE
                                </Button>
                              </Form.Item>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </StickyContainer>
                </Form>
              </div>
              <div className="col">
                {loading ? (
                  <div className="table-section spacing">
                    <div className="table-responsive">
                      <Spin size="large">
                        <Table
                          bordered={true}
                          columns={columns}
                          dataSource={tableData}
                        />
                      </Spin>
                    </div>
                  </div>
                ) : (
                  <div className="table-section spacing">
                    <div className="table-responsive">
                      <Table
                        bordered={true}
                        columns={columns}
                        dataSource={tableData}
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
      <UpdateBrand
        visible={visible}
        onCreate={onUpdate}
        onCancel={() => {
          setVisible(false);
        }}
      />
    </>
  );
};

const mapStateToProps = (state) => ({
  brands: state.brand.brands,
});

export default connect(mapStateToProps, { getBrands, addBrand, getBrand, updateBrand, deleteBrand })(ManageBrand);
