import React, { useEffect } from "react";
import { Modal, Form, Input } from "antd";
import { connect } from "react-redux";

const UpdateCompany = ({ single_company, visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (single_company != null) {
      single_company = single_company.title;
      form.setFieldsValue({
        title: single_company,
      });
    }
  }, [single_company]);
  return (
    <Modal
      visible={visible}
      title="Update Company"
      okText="Update"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please input title",
            },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  single_company: state.company.company,
});

export default connect(mapStateToProps)(UpdateCompany);
