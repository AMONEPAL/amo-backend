import React, { useEffect } from "react";
import { Modal, Form, Input } from "antd";
import { connect } from "react-redux";

const UpdateBrand = ({ single_brand, visible, onCreate, onCancel }) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (single_brand != null) {
      single_brand = single_brand.title;
      form.setFieldsValue({
        title: single_brand,
      });
    }
  }, [single_brand]);
  return (
    <Modal
      visible={visible}
      title="Update Brand"
      okText="Update"
      cancelText="Cancel"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            onCreate(values);
          })
          .catch((info) => {
            console.log("Validate Failed:", info);
          });
      }}
    >
      <Form form={form} layout="vertical" name="form_in_modal">
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please input title",
            },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  single_brand: state.brand.brand,
});

export default connect(mapStateToProps)(UpdateBrand);
