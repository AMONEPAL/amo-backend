import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import {
  Layout,
  Breadcrumb,
  Button,
  Select,
  PageHeader,
  Tabs,
  Form,
  Input,
} from "antd";
import { StickyContainer, Sticky } from "react-sticky";
import SideNav from "../dashboard/SideNav";
import TopHeader from "../dashboard/TopHeader";
import { Link } from "react-router-dom";

const { Content } = Layout;
const { TabPane } = Tabs;

const renderTabBar = (props, DefaultTabBar) => (
  <Sticky bottomOffset={80}>
    {({ style }) => (
      <DefaultTabBar
        {...props}
        className="site-custom-tab-bar"
        style={{ ...style }}
      />
    )}
  </Sticky>
);

const UserAddress = (props) => {
  const [billingForm] = Form.useForm();
  const [shippingForm] = Form.useForm();

  const onBillingFormFinish = (val) => {
    const data = {
      ...val,
      user: 1,
      type: "billing",
    };
    console.log(data);
  };

  const onBillingFormFinishFailed = (errorInfo) => {
    console.log("B-Failed:", errorInfo);
  };

  const onShippingFormFinish = (val) => {
    console.log(val);
  };

  const onShippingFormFinishFailed = (errorInfo) => {
    console.log("S-Failed:", errorInfo);
  };

  return (
    <>
      <Layout>
        <SideNav />
        <Layout className="site-layout">
          <TopHeader />
          <Content className="site-layout-background">
            <Breadcrumb className="admin-breadcrumb">
              <Breadcrumb.Item>User</Breadcrumb.Item>
              <Breadcrumb.Item>
                <Link to="/user-address">Manage User Address</Link>
              </Breadcrumb.Item>
            </Breadcrumb>
            <PageHeader
              title="Add User Address"
              className="site-page-header"
            ></PageHeader>
            <StickyContainer>
              <div className="tab-section spacing">
                <Tabs
                  defaultActiveKey="1"
                  type="card"
                  renderTabBar={renderTabBar}
                >
                  <TabPane tab="Billing Address" key="1">
                    <div className="tab-content">
                      <h5 className="tab-title">Billing Address</h5>
                      <div className="tab-inner-content">
                        <Form
                          layout="vertical"
                          form={billingForm}
                          name="billingForm"
                          onFinish={onBillingFormFinish}
                          onFinishFailed={onBillingFormFinishFailed}
                        >
                          <div className="row">
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    State
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="state"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input state!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    City
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="city"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input city!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Street
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="street"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input street!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Zip-Code
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="zipcode"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input zip-code!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-4">
                              <Form.Item label="Phone">
                                <Form.Item name="phone" noStyle>
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-2">
                              <Form.Item>
                                <Button
                                  type="primary"
                                  htmlType="submit"
                                  style={{ width: "100%" }}
                                >
                                  SAVE
                                </Button>
                              </Form.Item>
                            </div>
                          </div>
                        </Form>
                      </div>
                    </div>
                  </TabPane>
                  <TabPane tab="Shipping Address" key="2">
                    <div className="tab-content">
                      <h5 className="tab-title">Shipping Address</h5>
                      <div className="tab-inner-content">
                        <Form
                          layout="vertical"
                          form={shippingForm}
                          name="shippingForm"
                          onFinish={onShippingFormFinish}
                          onFinishFailed={onShippingFormFinishFailed}
                        >
                          <div className="row">
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    State
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="state"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input state!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    City
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="city"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input city!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Street
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="street"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input street!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                            <div className="col-md-4">
                              <Form.Item
                                label={
                                  <div>
                                    <span
                                      style={{ color: "red" }}
                                      className="mr-1"
                                    >
                                      *
                                    </span>
                                    Zip-Code
                                  </div>
                                }
                              >
                                <Form.Item
                                  name="zipcode"
                                  rules={[
                                    {
                                      required: true,
                                      message: "Please input zip-code!",
                                    },
                                  ]}
                                  noStyle
                                >
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-4">
                              <Form.Item label="Phone">
                                <Form.Item name="phone" noStyle>
                                  <Input />
                                </Form.Item>
                              </Form.Item>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-2">
                              <Form.Item>
                                <Button
                                  type="primary"
                                  htmlType="submit"
                                  style={{ width: "100%" }}
                                >
                                  SAVE
                                </Button>
                              </Form.Item>
                            </div>
                          </div>
                        </Form>
                      </div>
                    </div>
                  </TabPane>
                </Tabs>
              </div>
            </StickyContainer>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

const mapStateToProps = (state) => ({
  categories: state.category.categories,
  brands: state.brand.brands,
  generic_names: state.generic_name.generic_names,
  product_unit: state.product_unit.product_units,
});

export default connect(mapStateToProps, null)(UserAddress);
