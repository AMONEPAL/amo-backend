import React, { useState } from "react";
import { Layout, Menu, Popover } from "antd";
import { Link } from "react-router-dom";
import { RightOutlined } from "@ant-design/icons";

const { Sider } = Layout;

const sub1 = (
  <div className="menu-pop-over-box">
    <Menu theme="dark" mode="inline" className="sub-menu">
      <Menu.Item key="sub1-1" className="sub-menu-item">
        <Link to="/manage-products">Manage Products</Link>
      </Menu.Item>
      <Menu.Item key="sub1-2" className="sub-menu-item">
        <Link to="/manage-category">Manage Categories</Link>
      </Menu.Item>
      <Menu.Item key="sub1-3" className="sub-menu-item">
        <Link to="/manage-product-unit">Manage Product Units</Link>
      </Menu.Item>
      <Menu.Item key="sub1-4" className="sub-menu-item">
        <Link to="/manage-generic-name">Manage Generic Names</Link>
      </Menu.Item>
    </Menu>
  </div>
);

const sub2 = (
  <div className="menu-pop-over-box">
    <Menu theme="dark" mode="inline" className="sub-menu">
      <Menu.Item key="sub2-1" className="sub-menu-item">
        <Link to="/manage-brand">Manage Brands</Link>
      </Menu.Item>
      <Menu.Item key="sub2-2" className="sub-menu-item">
        <Link to="/manage-company">Manage Companies</Link>
      </Menu.Item>
    </Menu>
  </div>
);

const sub3 = (
  <div className="menu-pop-over-box">
    <Menu theme="dark" mode="inline" className="sub-menu">
      <Menu.Item key="sub3-1" className="sub-menu-item">
        <Link to="/manage-orders">Manage Orders</Link>
      </Menu.Item>
    </Menu>
  </div>
);

const sub6 = (
  <div className="menu-pop-over-box">
    <Menu theme="dark" mode="inline" className="sub-menu">
      <Menu.Item key="sub6-1" className="sub-menu-item">
        <Link to="/general-settings">General Setting</Link>
      </Menu.Item>
      <Menu.Item key="sub6-2" className="sub-menu-item">
        <Link to="/meta-tags-and-description">Meta Tags & Description</Link>
      </Menu.Item>
    </Menu>
  </div>
);

// const sub4 = (
//   <div className="menu-pop-over-box">
//     <Menu theme="dark" mode="inline" className="sub-menu">
//       <Menu.Item key="sub4-1" className="sub-menu-item">
//         <Link to="">Manage Stocks</Link>
//       </Menu.Item>
//     </Menu>
//   </div>
// );

const SideNav = (props) => {
  return (
    <>
      <Sider>
        <div className="logo-img logo-expand-true">
          <a href="#">
            <img src="../static/images/logofinal.png" className="logo-img" />
          </a>
        </div>
        <Menu className="sidebar-main-menu" theme="dark" mode="inline">
          <Menu.Item key="1">
            <Popover
              placement="rightTop"
              content={sub1}
              className="menu-pop-over"
            >
              <div className="menu-items">
                <div className="menu-item-name">Products</div>
                <div className="menu-item-arrow">
                  <RightOutlined className="menu-item-arrow-icon" />
                </div>
              </div>
            </Popover>
          </Menu.Item>
          <Menu.Item key="2">
            <Popover
              placement="rightTop"
              content={sub2}
              className="menu-pop-over"
            >
              <div className="menu-items">
                <div className="menu-item-name">Brands</div>
                <div className="menu-item-arrow">
                  <RightOutlined className="menu-item-arrow-icon" />
                </div>
              </div>
            </Popover>
          </Menu.Item>
          <Menu.Item key="3">
            <Popover
              placement="rightTop"
              content={sub3}
              className="menu-pop-over"
            >
              <div className="menu-items">
                <div className="menu-item-name">Orders</div>
                <div className="menu-item-arrow">
                  <RightOutlined className="menu-item-arrow-icon" />
                </div>
              </div>
            </Popover>
          </Menu.Item>
          <Menu.Item key="4">
              <div className="menu-items">
              <Link to="/customers">
                <div className="menu-item-name">
                  Customers
                </div>
              </Link>
                <div className="menu-item-arrow">
                  <RightOutlined className="menu-item-arrow-icon" />
                </div>
              </div>
          </Menu.Item>
          <Menu.Item key="5">
              <div className="menu-items">
              <Link to="/coupons">
                <div className="menu-item-name">
                  Coupons
                </div>
              </Link>
                <div className="menu-item-arrow">
                  <RightOutlined className="menu-item-arrow-icon" />
                </div>
              </div>
          </Menu.Item>
          <Menu.Item key="6">
            <Popover
              placement="rightTop"
              content={sub6}
              className="menu-pop-over"
            >
              <div className="menu-items">
                <div className="menu-item-name">Settings</div>
                <div className="menu-item-arrow">
                  <RightOutlined className="menu-item-arrow-icon" />
                </div>
              </div>
            </Popover>
          </Menu.Item>
        </Menu>
      </Sider>
    </>
  );
};

export default SideNav;
