import React, { Fragment, useEffect, useState } from "react";
import { Card, Statistic, Button, notification } from "antd";
import { connect } from "react-redux";
import { getProducts } from "../../actions/product";
import moment from "moment";

const ExpiryProducts = (props) => {
  const [inOneMonths, setInOneMonths] = useState(0);
  const [inOneWeek, setInOneWeek] = useState(0);
  const [expiredProducts, setExpiredProducts] = useState(0);
  useEffect(() => {
    props.getProducts();
  }, []);

  useEffect(() => {
    if (props.products != null) {
      let belowMonth = 0;
      let alreadyExpired = 0;
      let belowWeek = 0;
      const getDifference = (exp_date) => {
        return moment([
          exp_date.get("year"),
          exp_date.get("month"),
          exp_date.get("date"),
        ]).diff(
          moment([
            moment().get("year"),
            moment().get("month"),
            moment().get("date"),
          ]),
          "days"
        );
      };
      props.products.map((element) => {
        let exp_date = moment(element.expiry_date);
        if (getDifference(exp_date) <= 30 && getDifference(exp_date) > 0) {
          belowMonth++;
        }
        if (getDifference(exp_date) <= 0) {
          alreadyExpired++;
        }
        if (getDifference(exp_date) <= 7 && getDifference(exp_date) > 0) {
          belowWeek++;
        }
      });
      setInOneMonths(belowMonth);
      setExpiredProducts(alreadyExpired);
      setInOneWeek(belowWeek);
    }
  }, [props.products]);

  useEffect(() => {
    getNotification();
  }, [inOneWeek]);

  const getNotification = () => {
    if (inOneWeek > 0) {
      notification.warning({
        message: "Expiry Date",
        description:
          "Some Products are going to expire in 7 days. You might want to take a look at your products",
      });
    }
  };

  return (
    <Fragment>
      <Card
        title="Expiry Products"
        bordered={false}
        className="dashboard-cards"
      >
        <div className="row">
          <div className="col-6">
            <div className="expiry-inner-30">
              <Card bordered={false}>
                <Statistic
                  title="In 30 days"
                  value={inOneMonths}
                  valueStyle={{ color: "#fff" }}
                  className="dashboard-stats"
                />
              </Card>
            </div>
          </div>
          <div className="col-6">
            <div className="expiry-inner-0">
              <Card bordered={false}>
                <Statistic
                  title="Expired"
                  value={expiredProducts}
                  valueStyle={{ color: "#fff" }}
                  className="dashboard-stats"
                />
              </Card>
            </div>
          </div>
        </div>
        <div className="row">
          <a href="auth#/expiry-products">
            <Button style={{ marginTop: "10px", marginLeft: "15px" }}>
              Show Expiry Products
            </Button>
          </a>
        </div>
      </Card>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  products: state.product.products,
});

export default connect(mapStateToProps, { getProducts })(ExpiryProducts);
