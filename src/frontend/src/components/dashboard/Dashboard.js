import React from 'react'
import { Layout } from 'antd';
import SideNav from "./SideNav";
import TopHeader from "./TopHeader";
import ExpiryProducts from "./ExpiryProducts";
import LowStockProducts from "./LowStockProducts";

const { Content } = Layout;

const Dashboard = props => {
    return (
        <>
        <Layout>
          <SideNav className="side-nav-fixed"/>
        <Layout className="site-layout">
          <TopHeader />
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            <div className="row">
              <div className="col-6">
                <ExpiryProducts />
              </div>
              <div className="col-6">
                <LowStockProducts />
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
      </>
    )
}

export default Dashboard;