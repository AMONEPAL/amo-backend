import React, { Fragment, useEffect, useState } from "react";
import { Card, Statistic, Button, notification } from "antd";
import { connect } from "react-redux";
import { getProducts } from "../../actions/product";

const LowStockProducts = (props) => {
  const [stockBelowFifty, setStockBelowFifty] = useState(0);
  const [stockBelowTen, setStockBelowTen] = useState(0);
  const [outOfStock, setOutOfStock] = useState(0);
  useEffect(() => {
    props.getProducts();
  }, []);

  useEffect(() => {
    if (props.products != null) {
      let belowFifty = 0;
      let belowTen = 0;
      let depletedStock = 0;
      props.products.map((product) => {
        product.variation_set.map((variant) => {
          if (variant.inventory <= 50) {
            belowFifty++;
          }
          if (variant.inventory <= 10) {
            belowTen++;
          }
          if (variant.inventory <= 0) {
            depletedStock++;
          }
        });
      });
      setStockBelowFifty(belowFifty);
      setStockBelowTen(belowTen);
      setOutOfStock(depletedStock);
    }
  }, [props.products]);

  useEffect(() => {
    getNotification();
  }, [stockBelowTen]);

  const getNotification = () => {
    if (stockBelowTen > 0) {
      notification.warning({
        message: "Low Stock",
        description:
          "Some Products are running very low on stock. You might want to take a look at your inventory",
      });
    }
  };

  return (
    <Fragment>
      <Card
        title="Low Stock Products"
        bordered={false}
        className="dashboard-cards"
      >
        <div className="row">
          <div className="col-6">
            <div className="stock-inner-50">
              <Card bordered={false}>
                <Statistic
                  title="Below 50"
                  value={stockBelowFifty}
                  valueStyle={{ color: "#fff" }}
                  className="dashboard-stats"
                />
              </Card>
            </div>
          </div>
          <div className="col-6">
            <div className="stock-inner-0">
              <Card bordered={false}>
                <Statistic
                  title="Out of Stock"
                  value={outOfStock}
                  valueStyle={{ color: "#fff" }}
                  className="dashboard-stats"
                />
              </Card>
            </div>
          </div>
        </div>
        <div className="row">
          <a href="auth#/product-stock">
            <Button style={{ marginTop: "10px", marginLeft: "15px" }}>
              Show Stock
            </Button>
          </a>
        </div>
      </Card>
    </Fragment>
  );
};

const mapStateToProps = (state) => ({
  products: state.product.products,
});

export default connect(mapStateToProps, { getProducts })(LowStockProducts);
