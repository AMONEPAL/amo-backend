import React from "react";
import { Layout, Menu, Dropdown } from "antd";

import { connect } from "react-redux";
import { logout } from "../../actions/auth";

const { Header } = Layout;

const TopHeader = (props) => {
  const headermenu = (
    <Menu>
      <Menu.Item>
        <a href="#">My Profile</a>
      </Menu.Item>
      <Menu.Item>
        <a onClick={props.logout}>Logout</a>
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Header className="site-layout-background" style={{ padding: 0 }}>
        <div className="header-content">
          <ul>
            <li>
              <Dropdown overlay={headermenu}>
                <a className="nav-link" onClick={(e) => e.preventDefault()}>
                  <i className="fa fa-user-circle user-icon"></i>
                </a>
              </Dropdown>
            </li>
            <li>
              <a className="nav-link" onClick={(e) => e.preventDefault()}>
                <i className="fa fa-bell bell-icon"></i>
              </a>
            </li>
          </ul>
        </div>
      </Header>
    </>
  );
};

export default connect(null, { logout })(TopHeader);
