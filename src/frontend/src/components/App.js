import React, { Component } from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import PrivateRoute from "../components/common/PrivateRoute";
import { Provider } from "react-redux";
import Alert from "./common/Alert";
import AlertTemplate from "react-alert-template-basic";
import { Provider as AlertProvider } from "react-alert";
import store from "../store";
import "antd/dist/antd.css";
import "../../static/css/custom.css";

import Dashboard from "../components/dashboard/Dashboard";
import Login from "../components/auth/Login";
import ManageProducts from "../components/pages/products/ManageProducts";
import ManageCategory from "../components/product/ManageCategory";
import AddProduct from "../components/pages/products/AddProduct";
import ManageProductUnit from "../components/product/ManageProductUnit";
import ManageGenericName from "../components/product/ManageGenericName";
import AddProductUnit from "../components/product/AddProductUnit";
import ManageBrand from "../components/brand/ManageBrand";
import ManageCompany from "../components/brand/ManageCompany";
import ManageProductVariation from "../components/pages/products/ManageProductVariation";
import UserAddress from "../components/account/UserAddress";

import Category from "../components/product/Category";
import ManageOrder from "../components/order/ManageOrder";
import OrderDetail from "../components/order/OrderDetail";
import ProductForm from "../components/product/ProductForm";
import ProductExpiry from "../components/pages/products/ProductExpiry";
import ProductStock from "../components/pages/products/ProductStock";
import { loadUser } from "../actions/auth";
import Customers from "./pages/customer/Customers";
import Coupons from "./pages/coupons/Coupons";
import GeneralSettings from "./pages/settings/GeneralSettings";
import MetaDescription from "./pages/settings/MetaDescription";

const alertOptions = {
  timeout: 3000,
  position: "top center",
};

class App extends Component {
  // componentDidMount() {
  //   store.dispatch(loadUser());
  // }
  render() {
    return (
      <Provider store={store}>
        <AlertProvider template={AlertTemplate} {...alertOptions}>
          <Router>
            <Alert />
            <Switch>
              <PrivateRoute exact path="/" component={Dashboard} />
              <Route exact path="/login" component={Login} />
              <PrivateRoute
                exact
                path="/manage-products"
                component={ManageProducts}
              />
              <PrivateRoute
                exact
                path="/manage-products/add"
                component={AddProduct}
              />
              <PrivateRoute exact path="/category" component={Category} />
              <PrivateRoute exact path="/product" component={ProductForm} />
              <PrivateRoute
                exact
                path="/manage-product-unit"
                component={ManageProductUnit}
              />
              <PrivateRoute
                exact
                path="/manage-product-unit/add"
                component={AddProductUnit}
              />
              <PrivateRoute
                exact
                path="/manage-generic-name"
                component={ManageGenericName}
              />
              <PrivateRoute
                exact
                path="/manage-category"
                component={ManageCategory}
              />
              <PrivateRoute
                exact
                path="/manage-brand"
                component={ManageBrand}
              />
              <PrivateRoute
                exact
                path="/manage-company"
                component={ManageCompany}
              />
              <PrivateRoute
                exact
                path="/manage-product/variation/:productID"
                component={ManageProductVariation}
              />
              <PrivateRoute
                exact
                path="/user-address"
                component={UserAddress}
              />
              <PrivateRoute
                exact
                path="/manage-orders"
                component={ManageOrder}
              />
              <PrivateRoute
                exact
                path="/manage-orders/order/:orderId"
                component={OrderDetail}
              />
              <PrivateRoute
                exact
                path="/expiry-products"
                component={ProductExpiry}
              />
              <PrivateRoute
                exact
                path="/product-stock"
                component={ProductStock}
              />
              <PrivateRoute exact path="/customers" component={Customers} />
              <PrivateRoute exact path="/coupons" component={Coupons} />
              <PrivateRoute
                exact
                path="/general-settings"
                component={GeneralSettings}
              />
              <PrivateRoute
                exact
                path="/meta-tags-and-description"
                component={MetaDescription}
              />
            </Switch>
          </Router>
        </AlertProvider>
      </Provider>
    );
  }
}

export default App;

ReactDOM.render(<App />, document.getElementById("app"));
