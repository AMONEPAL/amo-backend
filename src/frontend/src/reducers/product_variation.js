import {
  GET_PRODUCT_VARIATIONS,
  ADD_PRODUCT_VARIATION,
} from "../actions/types";

const initialState = {
  product_variations: null,
  count: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCT_VARIATIONS:
      return {
        ...state,
        product_variations: action.payload.results,
        count: action.payload.count,
      };

    case ADD_PRODUCT_VARIATION:
      return {
        ...state,
      };

    default:
      return state;
  }
}
