import { GET_ALL_GENERIC_NAMES, ADD_GENERIC_NAME, GET_GENERIC_NAME_BY_ID, UPDATE_GENERIC_NAME, DELETE_GENERIC_NAME } from ".././actions/types";

const initialState = {
  generic_names: null,
  generic_name: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_GENERIC_NAMES:
      return {
        ...state,
        generic_names: action.payload.results,
      };

    case ADD_GENERIC_NAME:
      return {
        ...state,
        generic_names: [...state.generic_names, action.payload],
      };

    case GET_GENERIC_NAME_BY_ID:
      return {
        ...state,
        generic_name: action.payload,
      };

    case UPDATE_GENERIC_NAME:
      const item = state.generic_names.filter(
        (item) => item.id !== action.payload.id
      )
      return {
        ...state,
        generic_names: [action.payload, ...item],
      };

    case DELETE_GENERIC_NAME:
      return {
        ...state,
        generic_names: state.generic_names.filter(
          (item) => item.id !== action.payload
        ),
      };

    default:
      return state;
  }
}
