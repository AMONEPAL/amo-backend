import { GET_ALL_BRANDS, ADD_BRAND, GET_BRAND_BY_ID, UPDATE_BRAND, DELETE_BRAND } from ".././actions/types";

const initialState = {
  brands: null,
  brand: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_BRANDS:
      return {
        ...state,
        brands: action.payload.results,
      };

    case ADD_BRAND:
      return {
        ...state,
        brands: [...state.brands, action.payload],
      };

    case GET_BRAND_BY_ID:
      return {
        ...state,
        brand: action.payload,
      };

    case UPDATE_BRAND:
      const item = state.brands.filter(
        (item) => item.id !== action.payload.id
      )
      return {
        ...state,
        brands: [action.payload, ...item],
      };

    case DELETE_BRAND:
      return {
        ...state,
        brands: state.brands.filter(
          (item) => item.id !== action.payload
        ),
      };

    default:
      return state;
  }
}
