import { GET_ALL_CATEGORIES, ADD_CATEGORY } from ".././actions/types";

const initialState = {
  categories: null,
  category: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_CATEGORIES:
      return {
        ...state,
        categories: action.payload.results,
      };

    case ADD_CATEGORY:
      return {
        ...state,
      };

    default:
      return state;
  }
}
