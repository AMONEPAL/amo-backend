import { GET_ORDERS_LIST, GET_ORDER_BY_ID } from ".././actions/types";

const initialState = {
  orders: null,
  order: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ORDER_BY_ID:
      return {
        ...state,
        order: action.payload,
      };

    case GET_ORDERS_LIST:
      return {
        ...state,
        orders: action.payload.results,
      };

    default:
      return state;
  }
}
