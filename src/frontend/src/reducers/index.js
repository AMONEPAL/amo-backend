import { combineReducers } from "redux";
import alert from "./alert";
import auth from "./auth";
import product from "./product";
import category from "./category";
import brand from "./brand";
import product_unit from "./product_unit";
import generic_name from "./generic_name";
import company from "./company";
import product_variation from "./product_variation";

export default combineReducers({
  alert,
  auth,
  product,
  category,
  brand,
  product_unit,
  generic_name,
  company,
  product_variation,
});
