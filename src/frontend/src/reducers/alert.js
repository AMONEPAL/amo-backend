import { CREATE_MESSAGE, GET_MESSAGES, GET_ERRORS } from "../actions/types";
const initialState = {
  success_message: null,
  error_message: null,
  status: null,
};

export default function (state = initialState, actions) {
  switch (actions.type) {
    case CREATE_MESSAGE:
      return {
        ...state,
        success_message: actions.payload,
        error_message: null,
      };

    case GET_ERRORS:
      return {
        ...state,
        error_message: actions.payload.message,
        status: actions.payload.status,
        success_message: null,
      };

    default:
      return state;
  }
}
