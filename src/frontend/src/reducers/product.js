import {
  GET_ALL_PRODUCTS,
  ADD_PRODUCT,
  GET_PRODUCT_BY_ID,
} from ".././actions/types";

const initialState = {
  products: null,
  product: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_PRODUCTS:
      return {
        ...state,
        products: action.payload.results,
      };

    case GET_PRODUCT_BY_ID:
      return {
        ...state,
        product: action.payload,
      };

    default:
      return state;
  }
}
