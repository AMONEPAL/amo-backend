import {
  GET_ALL_PRODUCT_UNITS,
  GET_PRODUCT_UNIT_BY_ID,
  ADD_PRODUCT_UNIT,
  UPDATE_PRODUCT_UNIT,
  DELETE_PRODUCT_UNIT,
} from ".././actions/types";

const initialState = {
  product_units: null,
  product_unit: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_PRODUCT_UNITS:
      return {
        ...state,
        product_units: action.payload.results,
      };

    case ADD_PRODUCT_UNIT:
      return {
        ...state,
        product_units: [...state.product_units, action.payload],
      };

    case GET_PRODUCT_UNIT_BY_ID:
      return {
        ...state,
        product_unit: action.payload,
      };

    case UPDATE_PRODUCT_UNIT:
      const item = state.product_units.filter(
        (item) => item.id !== action.payload.id
      )
      return {
        ...state,
        product_units: [action.payload, ...item],
      };

    case DELETE_PRODUCT_UNIT:
      return {
        ...state,
        product_units: state.product_units.filter(
          (item) => item.id !== action.payload
        ),
      };

    default:
      return state;
  }
}
