import { GET_ALL_COMPANIES, ADD_COMPANY, GET_COMPANY_BY_ID, UPDATE_COMPANY, DELETE_COMPANY } from ".././actions/types";

const initialState = {
  companies: null,
  company: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_COMPANIES:
      return {
        ...state,
        companies: action.payload.results,
      };

    case ADD_COMPANY:
      return {
        ...state,
        companies: [...state.companies, action.payload],
      };

    case GET_COMPANY_BY_ID:
      return {
        ...state,
        company: action.payload,
      };

    case UPDATE_COMPANY:
      const item = state.companies.filter(
        (item) => item.id !== action.payload.id
      )
      return {
        ...state,
        companies: [action.payload, ...item],
      };

    case DELETE_COMPANY:
      return {
        ...state,
        companies: state.companies.filter(
          (item) => item.id !== action.payload
        ),
      };

    default:
      return state;
  }
}
