import axios from "axios";
import {
  GET_ALL_BRANDS,
  GET_BRAND_BY_ID,
  ADD_BRAND,
  UPDATE_BRAND,
  DELETE_BRAND,
} from "./types";
import { tokenConfig } from "./auth";
import { createMessage, returnErrors } from "./alert";

export const getBrands = () => (dispatch, getState) => {
  axios
    .get("/api/brands/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ALL_BRANDS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const addBrand = (data) => (dispatch, getState) => {
  axios
    .post("/api/brands/", data, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ addBrand: "Brand Added Successfully" })
      );
      dispatch({
        type: ADD_BRAND,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};


export const getBrand = (id) => (dispatch, getState) => {
  axios
    .get(`/api/brands/${id}`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_BRAND_BY_ID,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const updateBrand = (id, data) => (dispatch, getState) => {
  axios
    .put(`/api/brands/${id}/`, data, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ updateBrand: "Brand Updated Successfully" })
      );
      dispatch({
        type: UPDATE_BRAND,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const deleteBrand = (id) => (dispatch, getState) => {
  axios
    .delete(`/api/brands/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ deleteBrand: "Brand Deleted Successfully" })
      );
      dispatch({
        type: DELETE_BRAND,
        payload: id,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};