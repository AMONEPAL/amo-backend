import axios from "axios";
import {
  GET_PRODUCT_VARIATIONS,
  ADD_PRODUCT_VARIATION,
  UPDATE_PRODUCT_VARIATION,
} from "./types";
import { tokenConfig } from "./auth";
import { createMessage, returnErrors } from "./alert";

export const getProductVariations = (id) => (dispatch, getState) => {
  axios
    .get(`/api/variation/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_PRODUCT_VARIATIONS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const addProductVariation = (data) => (dispatch, getState) => {
  const body = JSON.stringify(data);
  axios
    .post("/api/variation_create_update/", body, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({
          addProductVariation: "Product Variation Added Successfully",
        })
      );
      dispatch({
        type: ADD_PRODUCT_VARIATION,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const updateProductVariation = (data) => (dispatch, getState) => {
  const body = JSON.stringify(data);
  axios
    .post("/api/variation_create_update/", body, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ updateProductVariation: "Updated Successfully" })
      );
      dispatch({
        type: ADD_PRODUCT_VARIATION,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};
