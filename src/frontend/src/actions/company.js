import axios from "axios";
import {
  GET_ALL_COMPANIES,
  GET_COMPANY_BY_ID,
  ADD_COMPANY,
  UPDATE_COMPANY,
  DELETE_COMPANY,
} from "./types";
import { tokenConfig } from "./auth";
import { createMessage, returnErrors } from "./alert";

export const getCompanies = () => (dispatch, getState) => {
  axios
    .get("/api/companies/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ALL_COMPANIES,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const addCompany = (data) => (dispatch, getState) => {
  axios
    .post("/api/companies/", data, tokenConfig(getState))
    .then((res) => {
      dispatch(createMessage({ addCompany: "Company Added Successfully" }));
      dispatch({
        type: ADD_COMPANY,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const getCompany = (id) => (dispatch, getState) => {
  axios
    .get(`/api/companies/${id}`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_COMPANY_BY_ID,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const updateCompany = (id, data) => (dispatch, getState) => {
  axios
    .put(`/api/companies/${id}/`, data, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ updateCompany: "Company Updated Successfully" })
      );
      dispatch({
        type: UPDATE_COMPANY,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const deleteCompany = (id) => (dispatch, getState) => {
  axios
    .delete(`/api/companies/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ deleteCompany: "Company Deleted Successfully" })
      );
      dispatch({
        type: DELETE_COMPANY,
        payload: id,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};