import {
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
} from "./types";
import axios from "axios";
import { createMessage, returnErrors } from "./alert";

export const login = (mobile, password) => (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const body = JSON.stringify({ mobile, password });
  dispatch({ type: USER_LOADING });
  axios
    .post("/api/auth/token/", body, config)
    .then((res) => {
      dispatch(createMessage({ loggedIn: "Logged In Successfully" }));
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("err: ", err);
      dispatch({
        type: LOGIN_FAIL,
      });
    });
};

export const logout = () => (dispatch, getState) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const token = getState().auth.token;

  if (token) {
    config.headers["Authorization"] = `JWT ${token}`;
  }

  axios
    .post("/api/user/checkout/", null, config)
    .then((res) => {
      dispatch(createMessage({ loggedOut: "Logged Out Successfully" }));

      dispatch({
        type: LOGOUT_SUCCESS,
      });
    })
    .catch((err) => {
      console.log("logout error: ", err);
    });
};

export const loadUser = () => (dispatch, getState) => {
  const token = getState().auth.token;
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  // if (token) {
  //   config.headers["Authorization"] = `JWT ${token}`;
  // User Loading
  dispatch({ type: USER_LOADING });
  axios
    .post("/api/auth/token/", config)
    .then((res) => {
      dispatch({
        type: USER_LOADED,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({
        type: AUTH_ERROR,
      });
    });
  // } else {
  //   dispatch({
  //     type: AUTH_ERROR,
  //   });
  // }
};

export const tokenConfig = (getState) => {
  const token = getState().auth.token;
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  if (token) {
    config.headers["Authorization"] = `JWT ${token}`;
  }
  return config;
};
