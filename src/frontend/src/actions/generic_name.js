import axios from "axios";
import {
  GET_ALL_GENERIC_NAMES,
  GET_GENERIC_NAME_BY_ID,
  ADD_GENERIC_NAME,
  UPDATE_GENERIC_NAME,
  DELETE_GENERIC_NAME,
} from "./types";
import { createMessage, returnErrors } from "./alert";
import { tokenConfig } from "./auth";

export const getGenericNames = () => (dispatch, getState) => {
  axios
    .get("/api/generic_names/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ALL_GENERIC_NAMES,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const addGenericName = (data) => (dispatch, getState) => {
  axios
    .post("/api/generic_names/", data, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ addGenericName: "Generic Name Added Successfully" })
      );
      dispatch({
        type: ADD_GENERIC_NAME,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const getGenericName = (id) => (dispatch, getState) => {
  axios
    .get(`/api/generic_names/${id}`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_GENERIC_NAME_BY_ID,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const updateGenericName = (id, data) => (dispatch, getState) => {
  axios
    .put(`/api/generic_names/${id}/`, data, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ updateGenericName: "Generic Name Updated Successfully" })
      );
      dispatch({
        type: UPDATE_GENERIC_NAME,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const deleteGenericName = (id) => (dispatch, getState) => {
  axios
    .delete(`/api/generic_names/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ deleteGenericName: "Generic Name Deleted Successfully" })
      );
      dispatch({
        type: DELETE_GENERIC_NAME,
        payload: id,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};
