import axios from "axios";
import {
  GET_ALL_PRODUCT_UNITS,
  GET_PRODUCT_UNIT_BY_ID,
  ADD_PRODUCT_UNIT,
  UPDATE_PRODUCT_UNIT,
  DELETE_PRODUCT_UNIT,
} from "./types";
import { tokenConfig } from "./auth";
import { createMessage, returnErrors } from "./alert";

export const getProductUnits = () => (dispatch, getState) => {
  axios
    .get("/api/product_units/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ALL_PRODUCT_UNITS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const addProductUnit = (data) => (dispatch, getState) => {
  const body = JSON.stringify(data);
  axios
    .post("/api/product_units/", body, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ addProductUnit: "Product Unit Added Successfully" })
      );
      dispatch({
        type: ADD_PRODUCT_UNIT,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error > ", err);
    });
};

export const getProductUnit = (id) => (dispatch, getState) => {
  axios
    .get(`/api/product_units/${id}`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_PRODUCT_UNIT_BY_ID,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const updateProductUnit = (id, data) => (dispatch, getState) => {
  axios
    .put(`/api/product_units/${id}/`, data, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ updateProductUnit: "Product Unit Updated Successfully" })
      );
      dispatch({
        type: UPDATE_PRODUCT_UNIT,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const deleteProductUnit = (id) => (dispatch, getState) => {
  axios
    .delete(`/api/product_units/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch(
        createMessage({ deleteProductUnit: "Product Unit Deleted Successfully" })
      );
      dispatch({
        type: DELETE_PRODUCT_UNIT,
        payload: id,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};