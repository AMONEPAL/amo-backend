import axios from "axios";
import {
  GET_ALL_CATEGORIES,
  GET_CATEGORY_BY_ID,
  ADD_CATEGORY,
  UPDATE_CATEGORY,
  DELETE_CATEGORY,
} from "./types";
import { tokenConfig } from "./auth";

export const getCategories = () => (dispatch, getState) => {
  axios
    .get("/api/categories/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ALL_CATEGORIES,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const addCategory = (data) => (dispatch, getState) => {
  axios
    .post("/api/categories/", data, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: ADD_CATEGORY,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};
