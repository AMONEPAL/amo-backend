import axios from "axios";
import { GET_ORDER_BY_ID, GET_ORDERS_LIST } from "./types";
import { tokenConfig } from "./auth";

export const getOrdersList = () => (dispatch, getState) => {
  axios
    .get("/api/orders_lists/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ORDERS_LIST,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const getOrderById = (id) => (dispatch, getState) => {
  axios
    .get(`/api/orders/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ORDER_BY_ID,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};
