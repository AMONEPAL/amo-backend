import axios from "axios";
import {
  GET_PRODUCT_BY_ID,
  GET_ALL_PRODUCTS,
  ADD_PRODUCT,
  UPDATE_PRODUCT,
  DELETE_PRODUCT,
} from "./types";
import { tokenConfig } from "./auth";

export const getProducts = () => (dispatch, getState) => {
  axios
    .get("/api/products/", tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_ALL_PRODUCTS,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const getProductById = (id) => (dispatch, getState) => {
  axios
    .get(`/api/products/${id}/`, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: GET_PRODUCT_BY_ID,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};

export const addProduct = (data) => (dispatch, getState) => {
  axios
    .post("/api/products_add/", data, tokenConfig(getState))
    .then((res) => {
      dispatch({
        type: ADD_PRODUCT,
        payload: res.data,
      });
    })
    .catch((err) => {
      console.log("error: ", err.response);
    });
};
