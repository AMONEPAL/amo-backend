from django.shortcuts import render
from .serializers import AppointmentSerializer
from .models import Appointment
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, ListAPIView,ListCreateAPIView, RetrieveAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework import status
# Create your views here.

class ApointmentAPI(ListCreateAPIView):
	serializer_class = AppointmentSerializer
	def get_queryset(self):
		user = self.request.user
		print(user)
		if user.is_superuser:
			appointments = Appointment.objects.all()
		else:
			appointments = Appointment.objects.filter(fk_auth_user_id=user.id)
		return appointments

	def post(self, request, *args, **kwargs):
		print(request.POST)
		user = self.request.user
		name = request.data.get('name')
		mobile_no = request.data.get('mobile_number')
		address = request.data.get('address')
		date = request.data.get('date')
		message = request.data.get('message')
		appointment_id = request.data.get('appointment_id')
		if appointment_id:
			appointment = Appointment.objects.get(pk=int(appointment_id))
			appointment.is_seen = True
			appointment.save()
			return Response({
						'status': True,
						'detail': 'Appointment record as been marked as approved'
						})

		if not name:
			return Response({"Fail": "Name required"}, status.HTTP_400_BAD_REQUEST)
		if not mobile_no :
			return Response({"Fail": "Mobile number is required"}, status.HTTP_400_BAD_REQUEST)
		if not address:
			return Response({"Fail": "Address Required"}, status.HTTP_400_BAD_REQUEST)
		if not date:
			return Response({"Fail": "Appointment Date is required"}, status.HTTP_400_BAD_REQUEST)
		if not message:
			return Response({"Fail": "Mesasge is required"}, status.HTTP_400_BAD_REQUEST)

		appointment = Appointment()
		appointment.name = name
		appointment.mobile_number = mobile_no
		appointment.address = address
		appointment.date = date
		appointment.message = message
		appointment.fk_auth_user_id = user
		appointment.save()
		return Response({
						'status': True,
						'detail': 'You appointment as been received. Thank you'
						})
