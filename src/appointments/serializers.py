from rest_framework import serializers
from .models import  Appointment
from django.conf import settings
from django.contrib.auth import get_user_model
import time
User = get_user_model()



class AppointmentSerializer(serializers.ModelSerializer):
	date = serializers.DateTimeField(read_only=True, format="%Y-%m-%d")
	class Meta:
		model = Appointment
		fields = [
			'id',
			'name',
			'mobile_number',
			'address',
			'date',
			'timestamp',
			'is_seen',
			'message',


		]
	











