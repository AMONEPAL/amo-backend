from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()
import datetime

# Create your models here.
DATE_INPUT_FORMATS = ['%d-%m-%Y']
class Appointment(models.Model):
	# inquiry = models.ManyToManyField(Inquiry)
	name = models.CharField(max_length=200, null=True, blank=True) 
	fk_auth_user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)               
	mobile_number = models.CharField(max_length=200, null=True, blank=True)
	address = models.CharField(max_length=200, null=True, blank=True)
	date = models.DateTimeField(default=datetime.date.today, null=True, blank=True)
	timestamp = models.DateTimeField(auto_now_add=True)
	message = models.CharField(max_length=1200, null=True, blank=True)
	is_seen = models.BooleanField(default=False, blank=True)

	def __str__(self):
	   return self.message
	class Meta:
	   ordering = ('timestamp',)